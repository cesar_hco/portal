package com.Portal.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.Portal.conexao.Conexao;
import com.Portal.interfaces.LoguinInterface;
import com.Portal.modelo.Funcionario;
import com.Portal.modelo.Usuario;



public class Loguindao implements LoguinInterface{

	Connection conexao;
	
	public Loguindao() {
		try {
			conexao = Conexao.getConexao();
			} catch (SQLException e) {
			System.out.println("Erro ao conectar com banco de dados" +
			e.getMessage());
			}	
		}
	
	
	public  List<Usuario> logar(String usuario, String senha) throws SQLException{
				
			StringBuilder sql = new StringBuilder();
			sql.append("select usuario , senha , usuarioativo from usuario where usuario = '").append(usuario).append("' and senha = '").append(senha).append("'");
										
			PreparedStatement comando = conexao.prepareStatement(sql.toString());
			ResultSet resultado = comando.executeQuery();
			
			ArrayList<Usuario> lista = new ArrayList<Usuario>();
					
			while (resultado.next()) {				
						
			Usuario usuarios = new Usuario();
			
			usuarios.setUsuario(resultado.getString("usuario"));
			usuarios.setSenha(resultado.getString("senha"));
			usuarios.setUsuarioAtivo(resultado.getBoolean("usuarioAtivo"));
			
			lista.add(usuarios);
			}
			return lista;
		
		}
	}
	

