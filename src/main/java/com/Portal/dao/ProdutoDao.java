﻿package com.Portal.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.Portal.conexao.Conexao;
import com.Portal.modelo.Produto;



public class ProdutoDao {

		
	Connection conexao;
	
	public ProdutoDao(){
	try {
	conexao = Conexao.getConexao();
	} catch (SQLException e) {
	System.out.println("Erro ao conectar com banco de dados" +
	e.getMessage());
	}
	}

	public void salvar(Produto entidade) throws SQLException{
		
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO produto ");
		sql.append("(descricao , tamanho , precobase , nomeproduto) VALUES (? ,? ,?,?)");
		
		Connection conexao = Conexao.getConexao();
		
		PreparedStatement consulta = conexao.prepareStatement(sql.toString());
		consulta.setString(1, entidade.getDescricao());
		consulta.setString(2, entidade.getTamanho());
		consulta.setDouble(3, entidade.getPrecobase());
		consulta.setString(4, entidade.getNomeProduto());
		consulta.executeUpdate();
	}

	public void alterar(Produto entidade) throws SQLException {

		
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE produto SET ");
		sql.append(" descricao = ?, ");
		sql.append("tamanho = ?, ");
		sql.append("precobase = ?, ");
		sql.append("nomeproduto = ?  ");
		sql.append("WHERE idprodutos = ?");
				
			Connection conexao = Conexao.getConexao();
			
			PreparedStatement consulta = conexao.prepareStatement(sql.toString());
			
			consulta.setString(1, entidade.getDescricao());
			consulta.setString(2, entidade.getTamanho());
			consulta.setDouble(3, entidade.getPrecobase());
			consulta.setString(4, entidade.getNomeProduto());
			consulta.setInt(5, entidade.getIdProduto());
			
			
			consulta.executeUpdate();
			
			
			}
		
	public void remover(Produto entidade) {
		//Estamos criando a string de SQL que ir? remover o cliente.
		String sql = "DELETE FROM produto WHERE idprodutos = ? ";
		try {
			Connection conexao = Conexao.getConexao();
			
			PreparedStatement consulta = conexao.prepareStatement(sql);
		consulta.setInt(1, entidade.getIdProduto());
		consulta.executeUpdate();
		} catch (SQLException e) {
		System.out.println("Erro ao realizar Delete: " + e.getMessage());
		}
		}

	public Produto obter(Serializable identificador) {
		Produto produto = null;
		//Estamos criando a string de SQL que ir? recuperar os dados de um cliente.
		String sql = "SELECT * FROM produto WHERE idprodutos = ?";
		try {
			Connection conexao = Conexao.getConexao();
			PreparedStatement consulta = conexao.prepareStatement(sql);
		consulta.setLong(1, (Long) identificador);
		ResultSet resultado = consulta.executeQuery();
		if(resultado.next()){
		produto = new Produto();
		produto.setIdProduto(resultado.getInt("idprodutos"));
		produto.setDescricao(resultado.getString("descricao"));
		produto.setTamanho(resultado.getString("tamanho"));
		produto.setPrecobase(resultado.getDouble("precobase"));
		produto.setNomeProduto(resultado.getString("nomeproduto"));
	
		
		}
		
		} catch (SQLException e) {
		System.out.println("Erro ao realizar Select: " + e.getMessage());
		}
		return produto;
		}

			

	public List<Produto> Consultar(String campoConsulta, String nome) {
		List<Produto> listaConsulta = new ArrayList<Produto>(0);
		if (campoConsulta.equals("DESCRICAO")) {
			try {
				listaConsulta = ConsultarDescricao(nome);
			} catch (SQLException e) {
				
				System.out.println("Erro ao realizar Select: " + e.getMessage());
			}
		}
				
		return listaConsulta;
	}
	
	public List<Produto> ConsultarDescricao(String nome ) throws SQLException {
				
		StringBuilder sql = new StringBuilder();
		sql.append("Select idprodutos,descricao , tamanho , nomeproduto , precobase ");
		sql.append("From produto ");
		sql.append(" where descricao ilike ('").append(nome).append("%') ");
		sql.append("ORDER BY idprodutos;");
		
								
		PreparedStatement comando = conexao.prepareStatement(sql.toString());
		ResultSet resultado = comando.executeQuery();
		
		ArrayList<Produto> lista = new ArrayList<Produto>();
				
		while (resultado.next()) {
		Produto produto = new Produto();
		produto.setIdProduto(resultado.getInt("idprodutos"));
		produto.setDescricao(resultado.getString("descricao"));
		produto.setTamanho(resultado.getString("tamanho"));
		produto.setNomeProduto(resultado.getString("nomeproduto"));
		produto.setPrecobase(resultado.getDouble("precobase"));
		
		lista.add(produto);
		}
		return lista;
	
	
	
	}
	
	public ArrayList<Produto> ConsultaCompleta() throws SQLException {
				
		StringBuilder sql = new StringBuilder();
		sql.append("Select idprodutos,descricao , tamanho , nomeproduto ");
		sql.append("From produto ");
				
		
								
		PreparedStatement comando = conexao.prepareStatement(sql.toString());
		ResultSet resultado = comando.executeQuery();
		
		ArrayList<Produto> lista = new ArrayList<Produto>();
				
		while (resultado.next()) {
		Produto produto = new Produto();
		produto.setIdProduto(resultado.getInt("idprodutos"));
		produto.setDescricao(resultado.getString("descricao"));
		produto.setTamanho(resultado.getString("tamanho"));
		produto.setNomeProduto(resultado.getString("nomeproduto"));
		
		lista.add(produto);
		}
		return lista;
	}
	}
	
	


