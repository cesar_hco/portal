package com.Portal.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.Portal.conexao.Conexao;
import com.Portal.modelo.Servico;


public class servicoDao {

	

		Connection conexao;

		public servicoDao() {
			
			try {
				conexao = Conexao.getConexao();
			} catch (SQLException e) {
				System.out.println("Erro ao conectar com banco de dados" + e.getMessage());
			}
		}
		
		public void salvar(Servico servico) throws SQLException{
			
			StringBuilder sql = new StringBuilder();
			
			sql.append("INSERT INTO servico ");
			sql.append(" descricaoservico , valorservico , tempoestimadoserviço , situacaoservico VALUES ( ? , ? , ? , ?)");
			
			PreparedStatement inserir = conexao.prepareStatement(sql.toString());
			
			inserir.setString(1 , servico.getDescricaoServico());
			inserir.setDouble(2, servico.getValorServico());
			inserir.setInt(3 , servico.getTempoEstimadoServico());
			inserir.setBoolean(4 , servico.isSituacaoServico());
			
			inserir.executeUpdate();
			ResultSet rs = inserir.getGeneratedKeys();
		    if (rs.next()) {
		    	servico.setIdServico(rs.getInt(1));
		    }
		}

		public ArrayList<Servico> listar() throws SQLException {
			StringBuilder sql = new StringBuilder();

			
			sql.append(" select idservico , descricaoservico , valorservico , tempoestimadoservico , situacaoservico from servico order by  descricaoservico ");
			

			Connection conexao = Conexao.getConexao();
			
			PreparedStatement listar = conexao.prepareStatement(sql.toString());

			ResultSet resultado = listar.executeQuery();

			ArrayList<Servico> lista = new ArrayList<Servico>();

			while (resultado.next()) {

				
				
				
				Servico servico = new Servico();

				servico.setIdServico(resultado.getInt("idServico"));
				servico.setDescricaoServico(resultado.getString("descricaoServico"));
				servico.setValorServico(resultado.getDouble("valorServico"));
				servico.setTempoEstimadoServico(resultado.getInt("tempoEstimadoServico"));
				servico.setSituacaoServico(resultado.getBoolean("situacaoServico"));
				
				lista.add(servico);
				
			}
			return lista;
		}

		public void excluir(Servico servico) throws SQLException {

			StringBuilder sql = new StringBuilder();

			sql.append("DELETE FROM servico ");
			sql.append("WHERE  idservico = ?  ");

			PreparedStatement deletar = conexao.prepareStatement(sql.toString());

			deletar.setInt(1, servico.getIdServico());

			deletar.executeUpdate();

		}
	public void alterar(Servico servico) throws SQLException {

			
			StringBuilder sql = new StringBuilder();
			
			sql.append("UPDATE servico SET ");
			sql.append(" descricaoservico = ? ,");
			sql.append(" valorservico = ? , ");
			sql.append(" tempoestimadoservico = ? ,");
			sql.append(" situacaoservico = ? , ");
			sql.append("WHERE idservico = ?");
			
			PreparedStatement alterar = conexao.prepareStatement(sql.toString());
			
			alterar.setString(1, servico.getDescricaoServico());
			alterar.setDouble(2 , servico.getValorServico());
			alterar.setInt(3 , servico.getTempoEstimadoServico());
			alterar.setBoolean(4 , servico.isSituacaoServico());
			
			
			alterar.executeUpdate();
			
	}
	
	}

