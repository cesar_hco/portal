package com.Portal.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.faces.context.FacesContext;

import com.Portal.conexao.MensagemAlertaEnum;




public abstract class Super implements Serializable {

	private static final long serialVersionUID = 1L;
	
	protected String mensagem = null;
	private String mensagemDetalhada = null;
	private String mensagemID = null;
	private Boolean apresentarMensagemSucesso;
	private Boolean apresentarMensagemAtencao;
	private Boolean apresentarMensagemErro;
	private String diretorioPastaWeb;
	private List listaConsultaVOs;
	private String valorConsulta;
	private String nome;
	private Integer codigo;
	private String usuario;
	private String senha;
	
	private static Factor facadeFactory;
	

	public Super() {
		super();
	}
	
	
	public int getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}


	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}


	@Autowired
	public void setFacadeFactory(Factor facadeFactory) {
		Super.facadeFactory = facadeFactory;
	}

	public static Factor getFacadeFactory() {
		return facadeFactory;
	}
	
		
	protected FacesContext context() {
		return (FacesContext.getCurrentInstance());
	}

	public String getMensagem() {
		if ((getMensagemID() != null) && (!getMensagemID().equals(""))) {
			mensagem = getMensagemInternalizacao(getMensagemID());
			return mensagem;
		}
		return "";
	}

	public void setMensagem(String mensagem , MensagemAlertaEnum mensagemAlerta) {
		this.mensagemID = mensagemID;
		this.mensagemDetalhada = "";
		if (mensagemAlerta.equals(MensagemAlertaEnum.SUCESSO)) {
			setApresentarMensagemSucesso(true);
			setApresentarMensagemAtencao(false);
			setApresentarMensagemErro(false);
		} else if (mensagemAlerta.equals(MensagemAlertaEnum.ATENCAO)) {
			setApresentarMensagemSucesso(false);
			setApresentarMensagemAtencao(true);
			setApresentarMensagemErro(false);
		}
	}

	public String getMensagemID() {
		if (mensagemID == null) {
			mensagemID = "";
		}
		return mensagemID;
	}

	public void setMensagemID(String mensagemID) {
		this.mensagemID = mensagemID;
		this.mensagemDetalhada = "";
	}
	
	

	public String getMensagemDetalhada() {
		if (mensagemDetalhada == null) {
			mensagemDetalhada = "";
		}
		return mensagemDetalhada;
	}

	public void setMensagemDetalhada(String mensagemDetalhada) {
		this.mensagemDetalhada = mensagemDetalhada;
	}

	protected String getMensagemInternalizacao(String mensagemID) {
		String mensagem = "(" + mensagemID + ") Mensagem não localizada nas propriedades de internalização.";
		ResourceBundle bundle = null;
		Locale locale = null;
		String nomeBundle = context().getApplication().getMessageBundle();
		if (nomeBundle != null) {
			locale = context().getViewRoot().getLocale();
			bundle = ResourceBundle.getBundle(nomeBundle, locale, getCurrentLoader(nomeBundle));
			try {
				mensagem = bundle.getString(mensagemID);
				return mensagem;
			} catch (MissingResourceException e) {
				return mensagem;
			}
		}
		return mensagem;
	}

	public Locale getLocale() {
		return context().getViewRoot().getLocale();
	}

	protected static ClassLoader getCurrentLoader(Object fallbackClass) {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		if (loader == null) {
			loader = fallbackClass.getClass().getClassLoader();
		}
		return loader;
	}
	
	public void setMensagemDetalhada(String mensagemID, String mensagemDetalhada) {
		this.mensagemID = mensagemID;
		this.mensagemDetalhada = tratarMensagemErroDetalhada(mensagemDetalhada);
		this.setApresentarMensagemErro(true);
	}

	public void setMensagemDetalhada(String mensagemID, String mensagemDetalhada1, Boolean sucesso) {
		if (sucesso) {
			setMensagemID("");
		} else {
			this.mensagemID = mensagemID;
		}
		this.mensagemDetalhada = tratarMensagemErroDetalhada(mensagemDetalhada1);
	}

	
	public String tratarMensagemErroDetalhada(String mensagemDetalhada) {
		String novaMensagem = mensagemDetalhada;
		// TODO Alberto 15/12/2010 Corrigido para aparecer a mensagem de erro do
		// código somente no console
		String erroCodigo = "Motivo do Erro: >>>" + mensagemDetalhada;
		try {
			if (mensagemDetalhada == null) {
				novaMensagem = "Aconteceu um problema inesperado, informe ao administrador e o mesmo será solucionado.";
				// mensagemID = "msg_erro_inesperado";
				throw new Exception("A mensagem detalhada 'e.getMessage()' no bloco try/catch esta nula devido a um NullPointerException!");
			}
			if ((novaMensagem.indexOf("duplicar chave viola") != -1) || (novaMensagem.indexOf("duplicate key") != -1)) {
				if (novaMensagem.lastIndexOf("_") != -1) {
					if (novaMensagem.contains("contapagar_nrdocumento")) {
						novaMensagem = "Já existe um registro com este valor para o campo Número Documento.";
					} else {
						String campo = novaMensagem.substring(novaMensagem.lastIndexOf("_") + 1, novaMensagem.length() - 1);
						novaMensagem = "Já existe um registro com este valor para o campo " + campo.toUpperCase() + ".";
					}
				} else {
					novaMensagem = "Já existe um registro gravados com estes valores (" + mensagemDetalhada + ")";
				}
			}
			if (novaMensagem.contains("paiz_nome_key")) {
				novaMensagem = "Já existe um País cadastrado com este nome.";
			}
			if ((novaMensagem.indexOf("violates foreign key constraint") != -1) || (novaMensagem.indexOf("de chave estrangeira") != -1)) {
				novaMensagem = "Este registro é referenciado por outro cadastro, por isto não pode ser excluído e/ou modificado.";
				// System.out.println();
				// System.out.println(erroCodigo + "<<<");
				// TODO Alberto 15/12/2010 Corrigido para aparecer a mensagem de
				// erro do código somente no console
			}

		} catch (Exception e) {
			// System.out.println("MENSAGEM => " + e.getMessage());
			;
		}
		return novaMensagem;
	}
	
	public Boolean getApresentarMensagem() {
		return !getMensagem().equals("");
	}
	
	public Boolean getApresentarMensagemDetalhada() {
		return !getMensagemDetalhada().equals("");
	}

	public Boolean getApresentarMensagemSucesso() {
		if (apresentarMensagemSucesso == null) {
			apresentarMensagemSucesso = false;
		}
		return apresentarMensagemSucesso;
	}

	public void setApresentarMensagemSucesso(Boolean apresentarMensagemSucesso) {
		this.apresentarMensagemSucesso = apresentarMensagemSucesso;
	}

	public Boolean getApresentarMensagemAtencao() {
		if (apresentarMensagemAtencao == null) {
			apresentarMensagemAtencao = false;
		}
		return apresentarMensagemAtencao;
	}

	public void setApresentarMensagemAtencao(Boolean apresentarMensagemAtencao) {
		this.apresentarMensagemAtencao = apresentarMensagemAtencao;
	}

	public Boolean getApresentarMensagemErro() {
		if (apresentarMensagemErro == null) {
			apresentarMensagemErro = false;
		}
		return apresentarMensagemErro;
	}

	public void setApresentarMensagemErro(Boolean apresentarMensagemErro) {
		this.apresentarMensagemErro = apresentarMensagemErro;
	}

	
	
	public List getListaConsultaVOs() {
		if (listaConsultaVOs == null) {
			listaConsultaVOs = new ArrayList<>(0);
		}
		return listaConsultaVOs;
	}

	public void setListaConsultaVOs(List listaConsultaVOs) {
		this.listaConsultaVOs = listaConsultaVOs;
	}

	public String getValorConsulta() {
		if (valorConsulta == null) {
			valorConsulta = "";
		}
		return valorConsulta;
	}

	public void setValorConsulta(String valorConsulta) {
		this.valorConsulta = valorConsulta;
	}

	public String getnome() {
		if (nome == null) {
			nome = "";
		}
		return nome;
	}

	public void setnome(String nome) {
		this.nome = nome;
	}


	public String getUsuario() {
		if (usuario == null) {
			usuario = "";
		}
		return usuario;
	}


	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}


	public String getSenha() {
		if (senha == null) {
			senha = "";
		}
		return senha;
	}


	public void setSenha(String senha) {
		this.senha = senha;
	}

	


}
