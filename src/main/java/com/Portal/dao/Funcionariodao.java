package com.Portal.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.Portal.conexao.Conexao;
import com.Portal.modelo.Funcionario;

public class Funcionariodao {

	Connection conexao;

	public Funcionariodao() {
		try {
			conexao = Conexao.getConexao();
		} catch (SQLException e) {
			System.out.println("Erro ao conectar com banco de dados" + e.getMessage());
		}
	}

	public void salvar(Funcionario funcionario) throws SQLException {

		StringBuilder sql = new StringBuilder();

		sql.append("INSERT INTO funcionario  ");
		sql.append("(cpfFuncionario,nomeFuncionario,nascimentoFuncionario ,sexofuncionario,emailfuncionario,telefonefuncionario ,celularfuncionario ,profissaofuncionario,cargofuncionario ,ativofuncionario)");
		sql.append("VALUES (?,?,?,?,?,?,?,?,?,?)");

		PreparedStatement comando = conexao.prepareStatement(sql.toString());

		comando.setString(1, funcionario.getCpfFuncionario());
		comando.setString(2, funcionario.getNomeFuncionario());
		comando.setDate(3, new java.sql.Date(funcionario.getNascimentoFuncionario().getTime()));
		comando.setString(4, funcionario.getSexofuncionario());
		comando.setString(5, funcionario.getEmailfuncionario());
		comando.setString(6, funcionario.getTelefonefuncionario());
		comando.setString(7, funcionario.getCelularfuncionario());
		comando.setString(8, funcionario.getProfissaoFuncionario());
		comando.setString(9, funcionario.getCargofuncionario());
		comando.setBoolean(10, funcionario.isAtivofuncionario());

		comando.executeUpdate();

	}

	public ArrayList<Funcionario> listar() throws SQLException {
		
		StringBuilder sql = new StringBuilder();

		sql.append("SELECT idfuncionario,cpfFuncionario,nomeFuncionario,nascimentoFuncionario ,sexofuncionario,emailfuncionario,telefonefuncionario ,celularfuncionario ,profissaofuncionario,cargofuncionario ,ativofuncionario ");
		sql.append("FROM funcionario Order By nomefuncionario ");
	
		PreparedStatement comando = conexao.prepareStatement(sql.toString());
		
		ResultSet resultado = comando.executeQuery();

		ArrayList<Funcionario> lista = new ArrayList<Funcionario>();

		while (resultado.next()) {

			Funcionario funcionario = new Funcionario();
			
			funcionario.setIdfuncionario(resultado.getInt("idfuncionario"));
			funcionario.setCpfFuncionario(resultado.getString("cpfFuncionario"));
			funcionario.setNomeFuncionario(resultado.getString("nomeFuncionario"));
			funcionario.setNascimentoFuncionario(resultado.getDate("nascimentoFuncionario"));
			funcionario.setSexofuncionario(resultado.getString("sexoFuncionario"));
			funcionario.setEmailfuncionario(resultado.getString("emailFuncionario"));
			funcionario.setTelefonefuncionario(resultado.getString("telefoneFuncionario"));
			funcionario.setCelularfuncionario(resultado.getString("celularFuncionario"));
			funcionario.setProfissaoFuncionario(resultado.getString("profissaoFuncionario"));
			funcionario.setCargofuncionario(resultado.getString("cargoFuncionario"));
			funcionario.setAtivofuncionario(resultado.getBoolean("ativoFuncionario"));
			
			lista.add(funcionario);
			
	}
 return lista;
	}
public void excluir(Funcionario funcionario) throws SQLException{
	
	StringBuilder sql = new StringBuilder();
	
	sql.append("Delete from funcionario where idfuncionario =? ");
	
	PreparedStatement comando = conexao.prepareStatement(sql.toString());
	
	comando.setInt(1, funcionario.getIdfuncionario());
	comando.executeUpdate();
}

public ArrayList<Funcionario> trazeritens() throws SQLException {
	
	StringBuilder sql = new StringBuilder();

	sql.append("SELECT idfuncionario,cpfFuncionario,nomeFuncionario,nascimentoFuncionario ,sexofuncionario,emailfuncionario,telefonefuncionario ,celularfuncionario ,profissaofuncionario,cargofuncionario ,ativofuncionario ");
	sql.append("FROM funcionario where idfuncionario = ? ");

	PreparedStatement comando = conexao.prepareStatement(sql.toString());
	
	ResultSet resultado = comando.executeQuery();

	ArrayList<Funcionario> lista = new ArrayList<Funcionario>();

	while (resultado.next()) {

		Funcionario funcionario = new Funcionario();
		
		funcionario.setIdfuncionario(resultado.getInt("idfuncionario"));
		funcionario.setCpfFuncionario(resultado.getString("cpfFuncionario"));
		funcionario.setNomeFuncionario(resultado.getString("nomeFuncionario"));
		funcionario.setNascimentoFuncionario(resultado.getDate("nascimentoFuncionario"));
		funcionario.setSexofuncionario(resultado.getString("sexoFuncionario"));
		funcionario.setEmailfuncionario(resultado.getString("emailFuncionario"));
		funcionario.setTelefonefuncionario(resultado.getString("telefoneFuncionario"));
		funcionario.setCelularfuncionario(resultado.getString("celularFuncionario"));
		funcionario.setProfissaoFuncionario(resultado.getString("profissaoFuncionario"));
		funcionario.setCargofuncionario(resultado.getString("cargoFuncionario"));
		funcionario.setAtivofuncionario(resultado.getBoolean("ativoFuncionario"));
		
		lista.add(funcionario);
		
}
return lista;
}
public void alterar(Funcionario funcionario) throws SQLException {

	StringBuilder sql = new StringBuilder();
	
	sql.append("UPDATE funcionario SET ");
	sql.append(" cpfFuncionario = ?, ");
	sql.append("nomeFuncionario = ?, ");
	sql.append("nascimentoFuncionario = ?, ");
	sql.append("sexofuncionario = ?, ");
	sql.append(" emailfuncionario = ?, ");
	sql.append("telefonefuncionario = ?, ");
	sql.append("celularfuncionario = ?, ");
	sql.append("profissaofuncionario = ?, ");
	sql.append(" cargofuncionario = ?, ");
	sql.append("ativofuncionario = ? ");
		
	sql.append("WHERE idfuncionario = ?");
	
	PreparedStatement comando = conexao.prepareStatement(sql.toString());
	
	comando.setString(1, funcionario.getCpfFuncionario());
	comando.setString(2, funcionario.getNomeFuncionario());
	comando.setDate(3, new java.sql.Date(funcionario.getNascimentoFuncionario().getTime()));
	comando.setString(4, funcionario.getSexofuncionario());
	comando.setString(5, funcionario.getEmailfuncionario());
	comando.setString(6,funcionario.getTelefonefuncionario());
	comando.setString(7, funcionario.getCelularfuncionario());
	comando.setString(8, funcionario.getProfissaoFuncionario());
	comando.setString(9, funcionario.getCargofuncionario());
	comando.setBoolean(10, funcionario.isAtivofuncionario());
	comando.setInt(11, funcionario.getIdfuncionario());
	
	comando.executeUpdate();
	
}
}