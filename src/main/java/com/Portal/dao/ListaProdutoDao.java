package com.Portal.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.Portal.conexao.Conexao;
import com.Portal.modelo.Cliente;
import com.Portal.modelo.ListaProduto;
import com.Portal.modelo.Orcamento;
import com.Portal.modelo.Produto;

public class ListaProdutoDao {

	Connection conexao;
	  
	public ListaProdutoDao() {
		try {
			conexao = Conexao.getConexao();
		} catch (SQLException e) {
			System.out.println("Erro ao conectar com banco de dados" + e.getMessage());
		}
	}
	 
	public void salvar(ListaProduto listaProduto) throws SQLException{
			
				
			StringBuffer sql = new StringBuffer();
			
			sql.append("Insert Into itensorcamentos (id_orcamento ) ");
			sql.append("values ((select max(idorcamento) from orcamento))");
			
			PreparedStatement consulta = conexao.prepareStatement(sql.toString());
			
		
	
		consulta.setInt(1,  listaProduto.getId_orcamento().getIdOrcamento());
		
		consulta.executeUpdate();
		
}
	
	public ArrayList<ListaProduto> listar() throws SQLException {
		StringBuilder sql = new StringBuilder();

		sql.append("select  o.idorcamento,o.dataOrcamento , o.cliente,c.nomefantasiacliente , o.valorOrcamento , o.prazoEntregaOrcamento ,o.observaçãoorcamento , o.realOrcamento , o.porcentagemOrcamento ,o.descontoOrcamento ,o.diaEntregaOrcamento ,o.acrescimoOrcamento ,o.autorizarOrcamento ,o.validadeOrcamento,");
 		sql.append("o.situacaoOrcamento ,o.condicaoPagamentoOrcamento, p.descricao , p.tamanho , p.precobase , p.idprodutos,p.nomeproduto FROM itensorcamentos ");
		sql.append("INNER JOIN orcamento o on itensorcamentos.id_orcamento = o.idorcamento ");
		sql.append("INNER JOIN produto p on itensorcamentos.id_produtos = p.idprodutos ");
		sql.append("INNER JOIN cliente c on o.cliente = c.idcliente ");
		Connection conexao = Conexao.getConexao();
		PreparedStatement comando = conexao.prepareStatement(sql.toString());

		ResultSet resultado = comando.executeQuery();

		ArrayList<ListaProduto> lista = new ArrayList<ListaProduto>();

		while (resultado.next()) {

			Cliente cliente = new Cliente();
			
			cliente.setNomeFantasiaCliente(resultado.getString("nomeFantasiaCliente"));
			
			Orcamento orcamento = new Orcamento();

						
			orcamento.setIdOrcamento(resultado.getInt("idOrcamento"));
			orcamento.setDataOrcamento(resultado.getDate("dataOrcamento"));
			orcamento.setCliente(cliente);
			orcamento.setValorOrcamento(resultado.getDouble("valorOrcamento"));
			orcamento.setPrazoEntregaOrcamento(resultado.getInt("prazoEntregaOrcamento"));
			orcamento.setObservacaoOrcamento(resultado.getString("observaçãoorcamento"));
			orcamento.setRealOrcamento(resultado.getBoolean("realOrcamento"));
			orcamento.setPorcentagemOrcamento(resultado.getBoolean("porcentagemOrcamento"));
			orcamento.setDescontoOrcamento(resultado.getDouble("descontoOrcamento"));
			orcamento.setAcrescimoOrcamento(resultado.getDouble("acrescimoOrcamento"));
			orcamento.setDiaEntregaOrcamento(resultado.getDate("diaEntregaOrcamento"));
			orcamento.setAutorizarOrcamento(resultado.getBoolean("autorizarOrcamento"));
			orcamento.setSituacaoOrcamento(resultado.getString("situacaoOrcamento"));
			orcamento.setCondicaoPagamentoOrcamento(resultado.getString("condicaoPagamentoOrcamento"));
			
			
			Produto  produto = new Produto();
			
			
			produto.setIdProduto(resultado.getInt("idProdutos"));
			produto.setDescricao(resultado.getString("descricao"));
			produto.setTamanho(resultado.getString("tamanho"));
			produto.setPrecobase(resultado.getDouble("precobase"));
			produto.setNomeProduto(resultado.getString("nomeProduto"));
			
			
			ListaProduto listaProduto = new ListaProduto();

			
			listaProduto.setId_produtos(produto);
			listaProduto.setId_orcamento(orcamento);
			
			
			lista.add(listaProduto);
		}
		return lista;
	}


public void alterar(ListaProduto listaProduto) throws SQLException {

		
		StringBuilder sql = new StringBuilder();
		
		sql.append("UPDATE itensorcamentos SET ");
		sql.append(" id_produtos = ?, ");
			
		sql.append("WHERE iditensorcamentos = ?");
		
		PreparedStatement comando = conexao.prepareStatement(sql.toString());
		
		
		comando.setInt(1, listaProduto.getId_produtos().getIdProduto());
		comando.setInt(2, listaProduto.getIdlistaProduto());
		
		
		comando.executeUpdate();
		
}


	}


