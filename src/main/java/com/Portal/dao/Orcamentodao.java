package com.Portal.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.Portal.conexao.Conexao;
import com.Portal.modelo.Cliente;
import com.Portal.modelo.ListaProduto;
import com.Portal.modelo.Orcamento;
import com.Portal.modelo.Produto;

public class Orcamentodao {

	Connection conexao;

	public Orcamentodao() {
		try {
			conexao = Conexao.getConexao();
		} catch (SQLException e) {
			System.out.println("Erro ao conectar com banco de dados" + e.getMessage());
		}
	}

	public void salvar(Orcamento orcamento, List<ListaProduto> itensProduto) throws SQLException {

		StringBuffer sql = new StringBuffer();

		sql.append("Insert Into orcamento ");
		sql.append(
				"( cliente , valorOrcamento , prazoEntregaOrcamento ,observacaoOrcamento , realOrcamento , porcentagemOrcamento ,descontoOrcamento ,acrescimoOrcamento ,autorizarOrcamento ,situacaoOrcamento ,condicaoPagamentoOrcamento ) ");
		sql.append("values (?,?,?,?,?,?,?,?,?,?,?)");

		PreparedStatement consulta = conexao.prepareStatement(sql.toString());

		consulta.setInt(1, orcamento.getCliente().getIdCliente());
		consulta.setDouble(2, orcamento.getValorOrcamento());
		consulta.setInt(3, orcamento.getPrazoEntregaOrcamento());
		consulta.setString(4, orcamento.getObservacaoOrcamento());
		consulta.setBoolean(5, orcamento.isRealOrcamento());
		consulta.setBoolean(6, orcamento.isPorcentagemOrcamento());
		consulta.setDouble(7, orcamento.getDescontoOrcamento());
		consulta.setDouble(8, orcamento.getAcrescimoOrcamento());
		consulta.setBoolean(9, orcamento.isAutorizarOrcamento());
		consulta.setString(10, orcamento.getSituacaoOrcamento());
		consulta.setString(11, orcamento.getCondicaoPagamentoOrcamento());

		consulta.execute();

		for (int posicao = 0; posicao < itensProduto.size(); posicao++) {

			StringBuffer sql2 = new StringBuffer();

			sql2.append("Insert Into itensorcamentos ");
			sql2.append(" (id_produtos , id_orcamento) ");
			sql2.append("values (? , (select max(idorcamento) from orcamento))");

			PreparedStatement consulta2 = conexao.prepareStatement(sql2.toString());

			ListaProduto itemVenda = itensProduto.get(posicao);

			itemVenda.setId_orcamento(orcamento);

			itemVenda.getId_produtos();

			consulta2.setInt(1, itemVenda.getId_produtos().getIdProduto());

			consulta2.execute();

		}

	}

	public ArrayList<ListaProduto> listar() throws SQLException {
		StringBuilder sql = new StringBuilder();

		sql.append(
				"select  o.idorcamento,o.dataOrcamento , o.cliente,c.nomefantasiacliente , o.valorOrcamento , o.prazoEntregaOrcamento ,o.observacaoorcamento , o.realOrcamento , o.porcentagemOrcamento ,o.descontoOrcamento ,o.diaEntregaOrcamento ,o.acrescimoOrcamento ,o.autorizarOrcamento ,");
		sql.append(
				"o.situacaoOrcamento ,o.condicaoPagamentoOrcamento, p.descricao , p.tamanho , p.precobase , p.idprodutos,p.nomeproduto FROM itensorcamentos ");
		sql.append("INNER JOIN orcamento o on itensorcamentos.id_orcamento = o.idorcamento ");
		sql.append("INNER JOIN produto p on itensorcamentos.id_produtos = p.idprodutos ");
		sql.append("INNER JOIN cliente c on o.cliente = c.idcliente ");
		Connection conexao = Conexao.getConexao();
		PreparedStatement comando = conexao.prepareStatement(sql.toString());

		ResultSet resultado = comando.executeQuery();

		ArrayList<ListaProduto> lista = new ArrayList<ListaProduto>();

		while (resultado.next()) {

			Cliente cliente = new Cliente();

			cliente.setNomeFantasiaCliente(resultado.getString("nomeFantasiaCliente"));

			Orcamento orcamento = new Orcamento();

			orcamento.setIdOrcamento(resultado.getInt("idOrcamento"));
			orcamento.setDataOrcamento(resultado.getDate("dataOrcamento"));
			orcamento.setCliente(cliente);
			orcamento.setValorOrcamento(resultado.getDouble("valorOrcamento"));
			orcamento.setPrazoEntregaOrcamento(resultado.getInt("prazoEntregaOrcamento"));
			orcamento.setObservacaoOrcamento(resultado.getString("observacaoorcamento"));
			orcamento.setRealOrcamento(resultado.getBoolean("realOrcamento"));
			orcamento.setPorcentagemOrcamento(resultado.getBoolean("porcentagemOrcamento"));
			orcamento.setDescontoOrcamento(resultado.getDouble("descontoOrcamento"));
			orcamento.setAcrescimoOrcamento(resultado.getDouble("acrescimoOrcamento"));
			orcamento.setDiaEntregaOrcamento(resultado.getDate("diaEntregaOrcamento"));
			orcamento.setAutorizarOrcamento(resultado.getBoolean("autorizarOrcamento"));
			orcamento.setSituacaoOrcamento(resultado.getString("situacaoOrcamento"));
			
			orcamento.setCondicaoPagamentoOrcamento(resultado.getString("condicaoPagamentoOrcamento"));

			Produto produto = new Produto();

			produto.setIdProduto(resultado.getInt("idProdutos"));
			produto.setDescricao(resultado.getString("descricao"));
			produto.setTamanho(resultado.getString("tamanho"));
			produto.setPrecobase(resultado.getDouble("precobase"));
			produto.setNomeProduto(resultado.getString("nomeProduto"));

			ListaProduto listaProduto = new ListaProduto();

			listaProduto.setId_produtos(produto);
			listaProduto.setId_orcamento(orcamento);

			lista.add(listaProduto);
		}
		return lista;
	}

	public void excluir(Orcamento orcamento) throws SQLException {

		StringBuilder sql = new StringBuilder();

		sql.append("DELETE FROM orcamento ");
		sql.append("WHERE  idorcamento = ? ");

		PreparedStatement comando = conexao.prepareStatement(sql.toString());

		comando.setInt(1, orcamento.getIdOrcamento());

		comando.executeUpdate();

	}

	public void alterar(Cliente cliente) throws SQLException {

		StringBuilder sql = new StringBuilder();

		sql.append("UPDATE cliente SET ");
		sql.append(" nomefantasiacliente = ?, ");
		sql.append("cnpj = ?, ");
		sql.append("emailempresa = ?, ");
		sql.append(" telefoneempresa = ?, ");
		sql.append("cepempresa = ?, ");
		sql.append("complementoempresa = ?, ");
		sql.append("numeroempresa = ?, ");
		sql.append(" cidadeempresa = ?, ");
		sql.append("siteempresa = ?, ");
		sql.append("ruaempresa = ?, ");
		sql.append("bairroempresa = ?, ");
		sql.append(" estadoempresa = ?, ");
		sql.append("nomeresponsavel = ?, ");
		sql.append("cpfresponsavel = ?, ");
		sql.append("nascimentoresponsavel = ?, ");
		sql.append("sexoresponsavel = ?, ");
		sql.append("emailresponsavel = ?, ");
		sql.append(" cargo = ?, ");
		sql.append("celularresponsavel = ?, ");
		sql.append("whatsappresponsavel = ?, ");
		sql.append("nomecliente = ? ");

		sql.append("WHERE idcliente = ?");

		PreparedStatement comando = conexao.prepareStatement(sql.toString());

		comando.setString(1, cliente.getNomeFantasiaCliente());
		comando.setString(2, cliente.getCnpj());
		comando.setString(3, cliente.getEmailEmpresa());
		comando.setString(4, cliente.getTelefoneEmpresa());
		comando.setString(5, cliente.getCepEmpresa());
		comando.setString(6, cliente.getComplementoEmpresa());
		comando.setInt(7, cliente.getNumeroEmpresa());
		comando.setString(8, cliente.getCidadeEmpresa());
		comando.setString(9, cliente.getSiteEmpresa());
		comando.setString(10, cliente.getRuaEmpresa());
		comando.setString(11, cliente.getBairroEmpresa());
		comando.setString(12, cliente.getEstadoEmpresa());
		comando.setString(13, cliente.getNomeResponsavel());
		comando.setString(14, cliente.getCpfResponsavel());
		comando.setDate(15, new java.sql.Date(cliente.getNascimentoResponsavel().getTime()));
		comando.setString(16, cliente.getSexoResponsavel());
		comando.setString(17, cliente.getEmailResponsavel());
		comando.setString(18, cliente.getCargo());
		comando.setString(19, cliente.getCelularResponsavel());
		comando.setString(20, cliente.getWhatsAppResponsavel());
		comando.setString(21, cliente.getNomeCliente());
		comando.setInt(22, cliente.getIdCliente());

		comando.executeUpdate();

	}

	public ArrayList<Orcamento> listarorcamento() throws SQLException {
		StringBuilder sql = new StringBuilder();

		sql.append(
				"select idorcamento, cliente ,c.nomefantasiacliente , valorOrcamento , prazoEntregaOrcamento ,observacaoOrcamento , realOrcamento , porcentagemOrcamento ,descontoOrcamento ,");
		sql.append(
				"acrescimoOrcamento ,autorizarOrcamento ,situacaoOrcamento ,condicaoPagamentoOrcamento from orcamento ");
		sql.append("INNER JOIN cliente c on cliente = c.idcliente ");

		Connection conexao = Conexao.getConexao();
		PreparedStatement comando = conexao.prepareStatement(sql.toString());

		ResultSet resultado = comando.executeQuery();

		ArrayList<Orcamento> lista = new ArrayList<Orcamento>();

		while (resultado.next()) {

			Cliente cliente = new Cliente();

			cliente.setNomeFantasiaCliente(resultado.getString("nomeFantasiaCliente"));

			Orcamento orcamento = new Orcamento();

			orcamento.setIdOrcamento(resultado.getInt("idOrcamento"));
			orcamento.setCliente(cliente);
			orcamento.setValorOrcamento(resultado.getDouble("valorOrcamento"));
			orcamento.setPrazoEntregaOrcamento(resultado.getInt("prazoEntregaOrcamento"));
			orcamento.setObservacaoOrcamento(resultado.getString("observacaoorcamento"));
			orcamento.setRealOrcamento(resultado.getBoolean("realOrcamento"));
			orcamento.setPorcentagemOrcamento(resultado.getBoolean("porcentagemOrcamento"));
			orcamento.setDescontoOrcamento(resultado.getDouble("descontoOrcamento"));
			orcamento.setAcrescimoOrcamento(resultado.getDouble("acrescimoOrcamento"));
			orcamento.setAutorizarOrcamento(resultado.getBoolean("autorizarOrcamento"));
			orcamento.setSituacaoOrcamento(resultado.getString("situacaoOrcamento"));
			orcamento.setCondicaoPagamentoOrcamento(resultado.getString("condicaoPagamentoOrcamento"));

			lista.add(orcamento);
		}
		return lista;
	}
}
