package com.Portal.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.Portal.conexao.Conexao;
import com.Portal.modelo.Cliente;
import com.Portal.modelo.Funcionario;
import com.Portal.modelo.Usuario;

public class usuarioDao {

		Connection conexao;
		
		public usuarioDao() {
			try {
				conexao = Conexao.getConexao();
				} catch (SQLException e) {
				System.out.println("Erro ao conectar com banco de dados" +
				e.getMessage());
				}	
			}
		
		public void salvar(Usuario usuario) throws SQLException{
			
			StringBuilder sql = new StringBuilder();
			
			sql.append("INSERT INTO usuario ");
			sql.append("(usuario , senha , funcionario , usuarioAtivo) VALUES (?,?,?,?)");
			
			Connection conexao = Conexao.getConexao();
			
			PreparedStatement inserir = conexao.prepareStatement(sql.toString());
			
			inserir.setString(1, usuario.getUsuario());
			inserir.setString(2, usuario.getSenha());
			inserir.setInt(3, usuario.getFuncionario().getIdfuncionario());
			inserir.setBoolean(4, usuario.isUsuarioAtivo());
			inserir.executeUpdate();
		
		}
		
		public void alterar(Usuario usuario) throws SQLException{
			
			StringBuilder sql = new StringBuilder();
			
			sql.append("UPDATE usuario SET ");
			sql.append("usuario = ?,");
			sql.append("senha = ?,");
			sql.append("usuarioAtivo = ?");
			
			Connection conexao = Conexao.getConexao();
			
			PreparedStatement alterar = conexao.prepareStatement(sql.toString());
			
			alterar.setString(1, usuario.getUsuario());
			alterar.setString(2, usuario.getSenha());
			alterar.setBoolean(3, usuario.isUsuarioAtivo());
			alterar.executeUpdate();
			
		}
		
		public void excluir(Usuario usuario)throws SQLException{
			
			StringBuilder sql = new StringBuilder();
			
			sql.append("DELETE FROM usuario ");
			sql.append("where idUsuario = ?");
			
			Connection conexao = Conexao.getConexao();			
			PreparedStatement excluir = conexao.prepareStatement(sql.toString());
			
			excluir.setInt(1, usuario.getIdUsuario());
			excluir.executeUpdate();
			
		}
		
		public List<Usuario> ConsultarFuncioanrio(String nome ) throws SQLException {
			
			StringBuilder sql = new StringBuilder();
			sql.append("select usuario.idusuario , usuario.usuario , usuario.senha , funcionario.nomeFuncionario , usuario.usuarioativo from usuario ");
			sql.append("inner join  funcionario  on usuario.funcionario = funcionario.idFuncionario");
			sql.append(" where funcionario.nomeFuncionario ilike ('").append(nome).append("%') ");
			sql.append("ORDER BY idusuario;");
			
									
			PreparedStatement comando = conexao.prepareStatement(sql.toString());
			ResultSet resultado = comando.executeQuery();
			
			ArrayList<Usuario> lista = new ArrayList<Usuario>();
					
			while (resultado.next()) {
				
				Funcionario funcionario = new Funcionario();
				
				funcionario.setNomeFuncionario(resultado.getString("nomeFuncionario"));
						
			Usuario usuario = new Usuario();
			usuario.setIdUsuario(resultado.getInt("idUsuario"));
			usuario.setUsuario(resultado.getString("usuario"));
			usuario.setSenha(resultado.getString("senha"));
			usuario.setFuncionario(funcionario);
			usuario.setUsuarioAtivo(resultado.getBoolean("usuarioAtivo"));
			
			lista.add(usuario);
			
			}
			return lista;
		
		}
			
}
