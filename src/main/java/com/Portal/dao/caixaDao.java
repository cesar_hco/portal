package com.Portal.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.Portal.conexao.Conexao;
import com.Portal.modelo.Caixa;
import com.Portal.modelo.Funcionario;

public class caixaDao {

Connection conexao;


	
	public caixaDao() {
		
		try {
			conexao = Conexao.getConexao();
		} catch (SQLException e) {
			System.out.println("Erro ao conectar com banco de dados" + e.getMessage());
		}
	}
	

	
	public void salvar(Caixa caixa) throws SQLException{
		
		StringBuilder sql = new StringBuilder();
		
		sql.append("INSERT INTO caixa ");
		sql.append("(funcionamentoCaixa ,Funcionario , nomeCaixa , saldoCaixa) VALUES ( ?, ?, ?, ? )");
		
		PreparedStatement inserir = conexao.prepareStatement(sql.toString());
		
		inserir.setString(1, caixa.getFuncionamentoCaixa());
		inserir.setInt(2, caixa.getResponsavelCaixa().getIdfuncionario());
		inserir.setString(3, caixa.getNomeCaixa());
		inserir.setDouble(4, caixa.getSaldoCaixa());
		
		
		inserir.executeUpdate();
		ResultSet rs = inserir.getGeneratedKeys();
	    if (rs.next()) {
	    	caixa.setIdCaixa(rs.getInt(1));
	    }
	}

	public ArrayList<Caixa> listar() throws SQLException {
		
		StringBuilder sql = new StringBuilder();

		
		sql.append(" select idcaixa, funcionamentoCaixa ,f.nomeFuncionario , nomeCaixa , saldoCaixa from caixa ");
		sql.append(" inner join Funcionario f on funcionario = idfuncionario order by  descricaoMedida ");
		

		Connection conexao = Conexao.getConexao();
		
		PreparedStatement listar = conexao.prepareStatement(sql.toString());

		ResultSet resultado = listar.executeQuery();

		ArrayList<Caixa> lista = new ArrayList<Caixa>();

		while (resultado.next()) {
		
			
			Funcionario funcionario = new Funcionario();
			
			funcionario.setIdfuncionario(resultado.getInt("idfuncionario"));
			funcionario.setNomeFuncionario(resultado.getString("nomefuncionario"));
			
			
			Caixa caixa = new Caixa();

			caixa.setIdCaixa(resultado.getInt("idcaixa"));	
			caixa.setFuncionamentoCaixa(resultado.getString("funcionamentocaixa"));
			caixa.setResponsavelCaixa(funcionario);
			caixa.setNomeCaixa(resultado.getString("nomecaixa"));
			caixa.setSaldoCaixa(resultado.getDouble("saldocaixa"));
			
			lista.add(caixa);
			
		}
		return lista;
	}

	public void excluir(Caixa caixa) throws SQLException {

		StringBuilder sql = new StringBuilder();

		sql.append("DELETE FROM caixa ");
		sql.append("WHERE  idcaixa = ?  ");

		PreparedStatement deletar = conexao.prepareStatement(sql.toString());

		deletar.setInt(1, caixa.getIdCaixa());

		deletar.executeUpdate();

	}
public void alterar(Caixa caixa) throws SQLException {

		
		StringBuilder sql = new StringBuilder();
		
		sql.append("UPDATE caixa SET ");
		sql.append(" funcionamentoCaixa = ?, ");
		sql.append(" Funcionario = ?, ");
		sql.append(" nomeCaixa = ?, ");
		sql.append(" saldoCaixa = ?, ");
		sql.append("WHERE idcaixa = ?");
		
		PreparedStatement alterar = conexao.prepareStatement(sql.toString());
		
		alterar.setString(1, caixa.getFuncionamentoCaixa());
		alterar.setInt(2, caixa.getResponsavelCaixa().getIdfuncionario());
		alterar.setString(3, caixa.getNomeCaixa());
		alterar.setDouble(4, caixa.getSaldoCaixa());
		
		
		
		alterar.executeUpdate();
		
}

	
	
	
	
	
}
