package com.Portal.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.Portal.conexao.Conexao;
import com.Portal.modelo.Medida;

public class medidaDao {

	Connection conexao;

	
	
	public medidaDao() {
		
		try {
			conexao = Conexao.getConexao();
		} catch (SQLException e) {
			System.out.println("Erro ao conectar com banco de dados" + e.getMessage());
		}
	}
	

	
	public void salvar(Medida medida) throws SQLException{
		
		StringBuilder sql = new StringBuilder();
		
		sql.append("INSERT INTO medida ");
		sql.append("(descricaoMedida ,siglaMedida)  VALUES ( ?, ? )");
		
		PreparedStatement inserir = conexao.prepareStatement(sql.toString());
		
		inserir.setString(1, medida.getDescricaoMedida());
		inserir.setString(2, medida.getSiglaMedida());
		
		
		inserir.executeUpdate();
		ResultSet rs = inserir.getGeneratedKeys();
	    if (rs.next()) {
	    	medida.setIdMedida(rs.getInt(1));
	    }
	}

	public ArrayList<Medida> listar() throws SQLException {
		
		StringBuilder sql = new StringBuilder();

		
		sql.append(" select idmedida , descricaoMedida ,siglaMedida from medida order by  descricaoMedida ");
		

		Connection conexao = Conexao.getConexao();
		
		PreparedStatement listar = conexao.prepareStatement(sql.toString());

		ResultSet resultado = listar.executeQuery();

		ArrayList<Medida> lista = new ArrayList<Medida>();

		while (resultado.next()) {
		
			
			
			Medida medida = new Medida();

			medida.setIdMedida(resultado.getInt("idmedida"));	
			medida.setDescricaoMedida(resultado.getString("descricaomedida"));
			medida.setSiglaMedida(resultado.getString("siglamedida"));
			
			
			lista.add(medida);
			
		}
		return lista;
	}

	public void excluir(Medida medida) throws SQLException {

		StringBuilder sql = new StringBuilder();

		sql.append("DELETE FROM medida ");
		sql.append("WHERE  idmedida = ?  ");

		PreparedStatement deletar = conexao.prepareStatement(sql.toString());

		deletar.setInt(1, medida.getIdMedida());

		deletar.executeUpdate();

	}
public void alterar(Medida medida) throws SQLException {

		
		StringBuilder sql = new StringBuilder();
		
		sql.append("UPDATE medida SET ");
		sql.append(" descricaomedida = ?, ");
		sql.append(" siglamedida = ?, ");
		sql.append("WHERE idmedida = ?");
		
		PreparedStatement alterar = conexao.prepareStatement(sql.toString());
		
		alterar.setString(1, medida.getDescricaoMedida());
		alterar.setString(2, medida.getSiglaMedida());
		
		
		
		alterar.executeUpdate();
		
}

	
	
	
}
