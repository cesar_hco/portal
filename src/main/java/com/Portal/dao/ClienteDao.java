package com.Portal.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.Portal.conexao.Conexao;
import com.Portal.modelo.Cliente;

public class ClienteDao {

	Connection conexao;

	public ClienteDao() {
		try {
			conexao = Conexao.getConexao();
		} catch (SQLException e) {
			System.out.println("Erro ao conectar com banco de dados" + e.getMessage());
		}
	}

	public void salvar(Cliente cliente) throws SQLException {

		StringBuilder sql = new StringBuilder();

		sql.append("INSERT INTO cliente ");
		sql.append("( nomefantasiacliente ,cnpj ,emailempresa , telefoneempresa ,cepempresa , complementoempresa ,numeroempresa,cidadeempresa,siteempresa,ruaempresa,bairroempresa,estadoempresa,  ");
		sql.append("nomeresponsavel , cpfresponsavel ,nascimentoresponsavel,sexoresponsavel , emailresponsavel,cargo,celularresponsavel, whatsappresponsavel,nomecliente )");
		sql.append("VALUES ( ? , ? , ? , ?, ? , ? , ? , ? , ? ,? , ? , ? , ? , ? ,? ,? , ? ,? , ? , ? , ? )");

		PreparedStatement consulta = conexao.prepareStatement(sql.toString());

		consulta.setString(1, cliente.getNomeFantasiaCliente());
		consulta.setString(2, cliente.getCnpj());
		
		consulta.setString(3, cliente.getEmailEmpresa());
		consulta.setString(4, cliente.getTelefoneEmpresa());
		consulta.setString(5, cliente.getCepEmpresa());
		consulta.setString(6, cliente.getComplementoEmpresa());
		consulta.setInt(7, cliente.getNumeroEmpresa());
		consulta.setString(8, cliente.getCidadeEmpresa());
		consulta.setString(9, cliente.getSiteEmpresa());
		consulta.setString(10, cliente.getRuaEmpresa());
		consulta.setString(11, cliente.getBairroEmpresa());
		consulta.setString(12, cliente.getEstadoEmpresa());
		consulta.setString(13, cliente.getNomeResponsavel());
		consulta.setString(14, cliente.getCpfResponsavel());
		consulta.setDate(15, new java.sql.Date (cliente.getNascimentoResponsavel().getTime()));
		consulta.setString(16, cliente.getSexoResponsavel());
		consulta.setString(17, cliente.getEmailResponsavel());
		consulta.setString(18, cliente.getCargo());
		consulta.setString(19, cliente.getCelularResponsavel());
		consulta.setString(20, cliente.getWhatsAppResponsavel());
		consulta.setString(21, cliente.getNomeCliente());
				

		consulta.executeUpdate();
		
		ResultSet rs = consulta.getGeneratedKeys();
	    if (rs.next()) {
	     cliente.setIdCliente(rs.getInt(1));
	    }
	}

	public ArrayList<Cliente> listar() throws SQLException {
		StringBuilder sql = new StringBuilder();

		sql.append("select  idcliente, nomefantasiacliente ,cnpj  ,emailempresa , telefoneempresa ,cepempresa , complementoempresa , numeroempresa,cidadeempresa,siteempresa,ruaempresa,bairroempresa,estadoempresa, ");
		sql.append("nomeresponsavel , cpfresponsavel ,nascimentoresponsavel,sexoresponsavel , emailresponsavel,cargo,celularresponsavel,  whatsappresponsavel,nomecliente ");
		sql.append("from cliente  ");

		Connection conexao = Conexao.getConexao();
		PreparedStatement comando = conexao.prepareStatement(sql.toString());

		ResultSet resultado = comando.executeQuery();

		ArrayList<Cliente> lista = new ArrayList<Cliente>();

		while (resultado.next()) {

			Cliente cliente = new Cliente();

			cliente.setIdCliente(resultado.getInt("idCliente"));
			cliente.setNomeFantasiaCliente(resultado.getString("nomefantasiacliente"));
			cliente.setCnpj(resultado.getString("cnpj"));
			
			cliente.setEmailEmpresa(resultado.getString("emailEmpresa"));
			cliente.setTelefoneEmpresa(resultado.getString("telefoneEmpresa"));
			cliente.setCepEmpresa(resultado.getString("cepEmpresa"));
			cliente.setComplementoEmpresa(resultado.getString("complementoEmpresa"));
			
			cliente.setNumeroEmpresa(resultado.getInt("numeroEmpresa"));
			cliente.setCidadeEmpresa(resultado.getString("cidadeEmpresa"));
			cliente.setSiteEmpresa(resultado.getString("siteEmpresa"));
			cliente.setRuaEmpresa(resultado.getString("ruaEmpresa"));
			cliente.setBairroEmpresa(resultado.getString("bairroEmpresa"));
			cliente.setEstadoEmpresa(resultado.getString("estadoEmpresa"));
			cliente.setNomeResponsavel(resultado.getString("nomeResponsavel"));
			cliente.setCpfResponsavel(resultado.getString("cpfResponsavel"));
			cliente.setNascimentoResponsavel(resultado.getDate("nascimentoResponsavel"));
			cliente.setSexoResponsavel(resultado.getString("sexoResponsavel"));
			cliente.setEmailResponsavel(resultado.getString("emailResponsavel"));
			cliente.setCargo(resultado.getString("cargo"));
			cliente.setCelularResponsavel(resultado.getString("celularResponsavel"));
			cliente.setWhatsAppResponsavel(resultado.getString("WhatsAppResponsavel"));
			cliente.setNomeCliente(resultado.getString("nomeCliente"));
			
			
			lista.add(cliente);
		}
		return lista;
	}

	public void excluir(Cliente cliente) throws SQLException {

		StringBuilder sql = new StringBuilder();

		sql.append("DELETE FROM cliente ");
		sql.append("WHERE  idcliente=? ");

		PreparedStatement comando = conexao.prepareStatement(sql.toString());

		comando.setInt(1, cliente.getIdCliente());

		comando.executeUpdate();

	}
public void alterar(Cliente cliente) throws SQLException {

		
		StringBuilder sql = new StringBuilder();
		
		sql.append("UPDATE cliente SET ");
		sql.append(" nomefantasiacliente = ?, ");
		sql.append("cnpj = ?, ");
		sql.append("emailempresa = ?, ");
		sql.append(" telefoneempresa = ?, ");
		sql.append("cepempresa = ?, ");
		sql.append("complementoempresa = ?, ");
		sql.append("numeroempresa = ?, ");
		sql.append(" cidadeempresa = ?, ");
		sql.append("siteempresa = ?, ");
		sql.append("ruaempresa = ?, ");
		sql.append("bairroempresa = ?, ");
		sql.append(" estadoempresa = ?, ");
		sql.append("nomeresponsavel = ?, ");
		sql.append("cpfresponsavel = ?, ");
		sql.append("nascimentoresponsavel = ?, ");
		sql.append("sexoresponsavel = ?, ");
		sql.append("emailresponsavel = ?, ");
		sql.append(" cargo = ?, ");
		sql.append("celularresponsavel = ?, ");
		sql.append("whatsappresponsavel = ?, ");
		sql.append("nomecliente = ? ");
		
		sql.append("WHERE idcliente = ?");
		
		PreparedStatement comando = conexao.prepareStatement(sql.toString());
		
		comando.setString(1, cliente.getNomeFantasiaCliente());
		comando.setString(2, cliente.getCnpj());
		comando.setString(3, cliente.getEmailEmpresa());
		comando.setString(4, cliente.getTelefoneEmpresa());
		comando.setString(5,cliente.getCepEmpresa());
		comando.setString(6, cliente.getComplementoEmpresa());
		comando.setInt(7, cliente.getNumeroEmpresa());
		comando.setString(8, cliente.getCidadeEmpresa());
		comando.setString(9, cliente.getSiteEmpresa());
		comando.setString(10, cliente.getRuaEmpresa());
		comando.setString(11, cliente.getBairroEmpresa());
		comando.setString(12, cliente.getEstadoEmpresa());
		comando.setString(13,cliente.getNomeResponsavel());
		comando.setString(14, cliente.getCpfResponsavel());
		comando.setDate(15, new java.sql.Date(cliente.getNascimentoResponsavel().getTime()));
		comando.setString(16, cliente.getSexoResponsavel());
		comando.setString(17, cliente.getEmailResponsavel());
		comando.setString(18, cliente.getCargo());
		comando.setString(19, cliente.getCelularResponsavel());
		comando.setString(20, cliente.getWhatsAppResponsavel());
		comando.setString(21, cliente.getNomeCliente());
		comando.setInt(22, cliente.getIdCliente());
		
		
		comando.executeUpdate();
		
}

public void remover(Cliente cliente) {
	
	String sql = "DELETE FROM cliente WHERE idcliente = ? ";
	try {
		Connection conexao = Conexao.getConexao();
		
		PreparedStatement consulta = conexao.prepareStatement(sql);
	consulta.setInt(1, cliente.getIdCliente());
	consulta.executeUpdate();
	} catch (SQLException e) {
	System.out.println("Erro ao realizar Delete: " + e.getMessage());
	}
	}
public ArrayList<Cliente> listarOrcamento() throws SQLException {
	StringBuilder sql = new StringBuilder();

	sql.append("select  idcliente, nomefantasiacliente ,cnpj  ,emailempresa , telefoneempresa ,cepempresa , complementoempresa , numeroempresa,cidadeempresa,siteempresa,ruaempresa,bairroempresa,estadoempresa, ");
	sql.append("nomeresponsavel , cpfresponsavel ,nascimentoresponsavel,sexoresponsavel , emailresponsavel,cargo,celularresponsavel,  whatsappresponsavel,nomecliente ");
	sql.append("from cliente where nomefantasiacliente = '?' ");

	Connection conexao = Conexao.getConexao();
	PreparedStatement comando = conexao.prepareStatement(sql.toString());

	ResultSet resultado = comando.executeQuery();

	ArrayList<Cliente> lista = new ArrayList<Cliente>();

	while (resultado.next()) {

		Cliente cliente = new Cliente();

		cliente.setIdCliente(resultado.getInt("idCliente"));
		cliente.setNomeFantasiaCliente(resultado.getString("nomefantasiacliente"));
		cliente.setCnpj(resultado.getString("cnpj"));
		
		cliente.setEmailEmpresa(resultado.getString("emailEmpresa"));
		cliente.setTelefoneEmpresa(resultado.getString("telefoneEmpresa"));
		cliente.setCepEmpresa(resultado.getString("cepEmpresa"));
		cliente.setComplementoEmpresa(resultado.getString("complementoEmpresa"));
		
		cliente.setNumeroEmpresa(resultado.getInt("numeroEmpresa"));
		cliente.setCidadeEmpresa(resultado.getString("cidadeEmpresa"));
		cliente.setSiteEmpresa(resultado.getString("siteEmpresa"));
		cliente.setRuaEmpresa(resultado.getString("ruaEmpresa"));
		cliente.setBairroEmpresa(resultado.getString("bairroEmpresa"));
		cliente.setEstadoEmpresa(resultado.getString("estadoEmpresa"));
		cliente.setNomeResponsavel(resultado.getString("nomeResponsavel"));
		cliente.setCpfResponsavel(resultado.getString("cpfResponsavel"));
		cliente.setNascimentoResponsavel(resultado.getDate("nascimentoResponsavel"));
		cliente.setSexoResponsavel(resultado.getString("sexoResponsavel"));
		cliente.setEmailResponsavel(resultado.getString("emailResponsavel"));
		cliente.setCargo(resultado.getString("cargo"));
		cliente.setCelularResponsavel(resultado.getString("celularResponsavel"));
		cliente.setWhatsAppResponsavel(resultado.getString("WhatsAppResponsavel"));
		cliente.setNomeCliente(resultado.getString("nomeCliente"));

		lista.add(cliente);
	}
	return lista;
}
public ArrayList<Cliente> buscarCodigo(int codigo) throws SQLException {
	StringBuilder sql = new StringBuilder();

	sql.append("select  idcliente, nomefantasiacliente ,cnpj  ,emailempresa , telefoneempresa ,cepempresa , complementoempresa , numeroempresa,cidadeempresa,siteempresa,ruaempresa,bairroempresa,estadoempresa, ");
	sql.append("nomeresponsavel , cpfresponsavel ,nascimentoresponsavel,sexoresponsavel , emailresponsavel,cargo,celularresponsavel,  whatsappresponsavel,nomecliente ");
	sql.append("from cliente  ");
	sql.append("where  idcliente = ('").append(codigo).append("')");

	Connection conexao = Conexao.getConexao();
	PreparedStatement comando = conexao.prepareStatement(sql.toString());

	ResultSet resultado = comando.executeQuery();

	ArrayList<Cliente> lista = new ArrayList<Cliente>();

	while (resultado.next()) {

		Cliente cliente = new Cliente();

		cliente.setIdCliente(resultado.getInt("idCliente"));
		cliente.setNomeFantasiaCliente(resultado.getString("nomefantasiacliente"));
		cliente.setCnpj(resultado.getString("cnpj"));
		
		cliente.setEmailEmpresa(resultado.getString("emailEmpresa"));
		cliente.setTelefoneEmpresa(resultado.getString("telefoneEmpresa"));
		cliente.setCepEmpresa(resultado.getString("cepEmpresa"));
		cliente.setComplementoEmpresa(resultado.getString("complementoEmpresa"));
		
		cliente.setNumeroEmpresa(resultado.getInt("numeroEmpresa"));
		cliente.setCidadeEmpresa(resultado.getString("cidadeEmpresa"));
		cliente.setSiteEmpresa(resultado.getString("siteEmpresa"));
		cliente.setRuaEmpresa(resultado.getString("ruaEmpresa"));
		cliente.setBairroEmpresa(resultado.getString("bairroEmpresa"));
		cliente.setEstadoEmpresa(resultado.getString("estadoEmpresa"));
		cliente.setNomeResponsavel(resultado.getString("nomeResponsavel"));
		cliente.setCpfResponsavel(resultado.getString("cpfResponsavel"));
		cliente.setNascimentoResponsavel(resultado.getDate("nascimentoResponsavel"));
		cliente.setSexoResponsavel(resultado.getString("sexoResponsavel"));
		cliente.setEmailResponsavel(resultado.getString("emailResponsavel"));
		cliente.setCargo(resultado.getString("cargo"));
		cliente.setCelularResponsavel(resultado.getString("celularResponsavel"));
		cliente.setWhatsAppResponsavel(resultado.getString("WhatsAppResponsavel"));
		cliente.setNomeCliente(resultado.getString("nomeCliente"));
		
		
		lista.add(cliente);
	}
	return lista;
}
}