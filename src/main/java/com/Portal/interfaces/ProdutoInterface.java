package com.Portal.interfaces;

import java.sql.SQLException;
import java.util.List;

import com.Portal.modelo.Produto;

public interface ProdutoInterface {

	List<Produto>Consultar(String  campoConsulta , String nome ) throws SQLException ;

	List<Produto> ConsultarDescricao(String nome) throws SQLException;


	
		
	
}
