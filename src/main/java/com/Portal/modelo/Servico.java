package com.Portal.modelo;

public class Servico {

	public Integer idServico;
	public String descricaoServico;
	public Double valorServico;
	public int tempoEstimadoServico;
	public boolean situacaoServico;
	
	public Integer getIdServico() {
		return idServico;
	}
	public void setIdServico(Integer idServico) {
		this.idServico = idServico;
	}
	public String getDescricaoServico() {
		return descricaoServico;
	}
	public void setDescricaoServico(String descricaoServico) {
		this.descricaoServico = descricaoServico;
	}
	public Double getValorServico() {
		return valorServico;
	}
	public void setValorServico(Double valorServico) {
		this.valorServico = valorServico;
	}
	public int getTempoEstimadoServico() {
		return tempoEstimadoServico;
	}
	public void setTempoEstimadoServico(int tempoEstimadoServico) {
		this.tempoEstimadoServico = tempoEstimadoServico;
	}
	public boolean isSituacaoServico() {
		return situacaoServico;
	}
	public void setSituacaoServico(boolean situacaoServico) {
		this.situacaoServico = situacaoServico;
	}
	
	
	
}
