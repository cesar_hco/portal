package com.Portal.modelo;

public class ListaProduto {

	public Integer idlistaProduto;
	public Produto id_produtos;
	public Orcamento id_orcamento;
	public int quantidadeProduto;

	public int getQuantidadeProduto() {
		return quantidadeProduto;
	}

	public void setQuantidadeProduto(int quantidadeProduto) {
		this.quantidadeProduto = quantidadeProduto;
	}

	
	public Produto getId_produtos() {
		return id_produtos;
	}

	public void setId_produtos(Produto id_produtos) {
		this.id_produtos = id_produtos;
	}

	public Orcamento getId_orcamento() {
		return id_orcamento;
	}

	public void setId_orcamento(Orcamento id_orcamento) {
		this.id_orcamento = id_orcamento;
	}

	public int getIdlistaProduto() {
	
		if (idlistaProduto == null) {
			idlistaProduto = 0;
		}
		
		return idlistaProduto;
	}

	public void setIdlistaProduto(int idlistaProduto) {
		this.idlistaProduto = idlistaProduto;
	}

	

	

	
}
