package com.Portal.modelo;

public class Usuario {

	private Integer idUsuario;
	private String Usuario;
	private String senha;
	private Funcionario funcionario;
	private boolean usuarioAtivo;
	
	
	
	public Integer getIdUsuario() {
		if (idUsuario == null) {
			idUsuario = 0;
		}
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getUsuario() {
		if (Usuario == null) {
			Usuario = "";
		}
		return Usuario;
	}
	public void setUsuario(String usuario) {
		Usuario = usuario;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public Funcionario getFuncionario() {
		if (funcionario == null) {
			funcionario = new Funcionario();
		}
		return funcionario;
	}
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	public boolean isUsuarioAtivo() {
		return usuarioAtivo;
	}
	public void setUsuarioAtivo(boolean usuarioAtivo) {
		this.usuarioAtivo = usuarioAtivo;
	}
	
	
	
	
	
	
}
