package com.Portal.modelo;

public class Caixa {

	public Integer idCaixa;
	public Funcionario responsavelCaixa;
	public String funcionamentoCaixa;
	public String nomeCaixa;
	public Double saldoCaixa;
	
	
	public String getNomeCaixa() {
		return nomeCaixa;
	}
	public void setNomeCaixa(String nomeCaixa) {
		this.nomeCaixa = nomeCaixa;
	}
	public Double getSaldoCaixa() {
		return saldoCaixa;
	}
	public void setSaldoCaixa(Double saldoCaixa) {
		this.saldoCaixa = saldoCaixa;
	}
	public Integer getIdCaixa() {
		return idCaixa;
	}
	public void setIdCaixa(Integer idCaixa) {
		this.idCaixa = idCaixa;
	}
	public Funcionario getResponsavelCaixa() {
		return responsavelCaixa;
	}
	public void setResponsavelCaixa(Funcionario responsavelCaixa) {
		this.responsavelCaixa = responsavelCaixa;
	}
	public String getFuncionamentoCaixa() {
		return funcionamentoCaixa;
	}
	public void setFuncionamentoCaixa(String funcionamentoCaixa) {
		this.funcionamentoCaixa = funcionamentoCaixa;
	}
	
	
	
}
