package com.Portal.modelo;

import java.util.Date;

public class Funcionario {

	public int idfuncionario;
	public String cpfFuncionario;
	public String nomeFuncionario;
	public String sexofuncionario;
	public String emailfuncionario;
	public Date nascimentoFuncionario;
	public String telefonefuncionario;
	public String celularfuncionario;
	public String profissaoFuncionario;
	public String cargofuncionario;
	public boolean ativofuncionario;
	
	public int getIdfuncionario() {
		return idfuncionario;
	}
	public void setIdfuncionario(int idfuncionario) {
		this.idfuncionario = idfuncionario;
	}
	public String getCpfFuncionario() {
		return cpfFuncionario;
	}
	public void setCpfFuncionario(String cpfFuncionario) {
		this.cpfFuncionario = cpfFuncionario;
	}
	public String getNomeFuncionario() {
		return nomeFuncionario;
	}
	public void setNomeFuncionario(String nomeFuncionario) {
		this.nomeFuncionario = nomeFuncionario;
	}
	public String getSexofuncionario() {
		return sexofuncionario;
	}
	public void setSexofuncionario(String sexofuncionario) {
		this.sexofuncionario = sexofuncionario;
	}
	public String getEmailfuncionario() {
		return emailfuncionario;
	}
	public void setEmailfuncionario(String emailfuncionario) {
		this.emailfuncionario = emailfuncionario;
	}
	public Date getNascimentoFuncionario() {
		return nascimentoFuncionario;
	}
	public void setNascimentoFuncionario(Date nascimentoFuncionario) {
		this.nascimentoFuncionario = nascimentoFuncionario;
	}
	public String getTelefonefuncionario() {
		return telefonefuncionario;
	}
	public void setTelefonefuncionario(String telefonefuncionario) {
		this.telefonefuncionario = telefonefuncionario;
	}
	public String getCelularfuncionario() {
		return celularfuncionario;
	}
	public void setCelularfuncionario(String celularfuncionario) {
		this.celularfuncionario = celularfuncionario;
	}
	
	public String getCargofuncionario() {
		return cargofuncionario;
	}
	public void setCargofuncionario(String cargofuncionario) {
		this.cargofuncionario = cargofuncionario;
	}
	public String getProfissaoFuncionario() {
		return profissaoFuncionario;
	}
	public void setProfissaoFuncionario(String profissaoFuncionario) {
		this.profissaoFuncionario = profissaoFuncionario;
	}
	public boolean isAtivofuncionario() {
		return ativofuncionario;
	}
	public void setAtivofuncionario(boolean ativofuncionario) {
		this.ativofuncionario = ativofuncionario;
	}
	
	
}
