package com.Portal.modelo;

import java.util.Date;

public class Cliente {

public Integer idCliente;
public String nomeFantasiaCliente;
public String imagemCliente;
public String urlCliente;
public String cnpj;
public Date dataCadastro;
public String emailEmpresa;
public String telefoneEmpresa;
public String cepEmpresa;
public String complementoEmpresa;
public String nomeCliente;
public Integer numeroEmpresa;
public String cidadeEmpresa;
public String siteEmpresa;
public String ruaEmpresa;
public String bairroEmpresa;

public String estadoEmpresa;
public String nomeResponsavel;
public String cpfResponsavel;
public Date nascimentoResponsavel;
public String sexoResponsavel;
public String emailResponsavel;
public String cargo;
public String celularResponsavel;
public String WhatsAppResponsavel;

public Integer getIdCliente() {
	if (idCliente == null) {
		idCliente = 0;
	}
	return idCliente;
}
public void setIdCliente(Integer idCliente) {
	this.idCliente = idCliente;
}
public String getNomeFantasiaCliente() {
	return nomeFantasiaCliente;
}
public void setNomeFantasiaCliente(String nomeFantasiaCliente) {
	this.nomeFantasiaCliente = nomeFantasiaCliente;
}
public String getImagemCliente() {
	return imagemCliente;
}
public void setImagemCliente(String imagemCliente) {
	this.imagemCliente = imagemCliente;
}
public String getUrlCliente() {
	return urlCliente;
}
public void setUrlCliente(String urlCliente) {
	this.urlCliente = urlCliente;
}
public String getCnpj() {
	return cnpj;
}
public void setCnpj(String cnpj) {
	this.cnpj = cnpj;
}

public Date getDataCadastro() {
	return dataCadastro;
}
public void setDataCadastro(Date dataCadastro) {
	this.dataCadastro = dataCadastro;
}
public String getEmailEmpresa() {
	return emailEmpresa;
}
public void setEmailEmpresa(String emailEmpresa) {
	this.emailEmpresa = emailEmpresa;
}
public String getTelefoneEmpresa() {
	return telefoneEmpresa;
}
public void setTelefoneEmpresa(String telefoneEmpresa) {
	this.telefoneEmpresa = telefoneEmpresa;
}
public String getCepEmpresa() {
	return cepEmpresa;
}
public void setCepEmpresa(String cepEmpresa) {
	this.cepEmpresa = cepEmpresa;
}
public String getComplementoEmpresa() {
	return complementoEmpresa;
}
public void setComplementoEmpresa(String complementoEmpresa) {
	this.complementoEmpresa = complementoEmpresa;
}
public String getNomeCliente() {
	return nomeCliente;
}
public void setNomeCliente(String nomeCliente) {
	this.nomeCliente = nomeCliente;
}

public Integer getNumeroEmpresa() {
	return numeroEmpresa;
}
public void setNumeroEmpresa(Integer numeroEmpresa) {
	this.numeroEmpresa = numeroEmpresa;
}
public String getCidadeEmpresa() {
	return cidadeEmpresa;
}
public void setCidadeEmpresa(String cidadeEmpresa) {
	this.cidadeEmpresa = cidadeEmpresa;
}
public String getSiteEmpresa() {
	return siteEmpresa;
}
public void setSiteEmpresa(String siteEmpresa) {
	this.siteEmpresa = siteEmpresa;
}
public String getRuaEmpresa() {
	return ruaEmpresa;
}
public void setRuaEmpresa(String ruaEmpresa) {
	this.ruaEmpresa = ruaEmpresa;
}
public String getBairroEmpresa() {
	return bairroEmpresa;
}
public void setBairroEmpresa(String bairroEmpresa) {
	this.bairroEmpresa = bairroEmpresa;
}
public String getEstadoEmpresa() {
	return estadoEmpresa;
}
public void setEstadoEmpresa(String estadoEmpresa) {
	this.estadoEmpresa = estadoEmpresa;
}
public String getNomeResponsavel() {
	return nomeResponsavel;
}
public void setNomeResponsavel(String nomeResponsavel) {
	this.nomeResponsavel = nomeResponsavel;
}
public String getCpfResponsavel() {
	return cpfResponsavel;
}
public void setCpfResponsavel(String cpfResponsavel) {
	this.cpfResponsavel = cpfResponsavel;
}
public Date getNascimentoResponsavel() {
	return nascimentoResponsavel;
}
public void setNascimentoResponsavel(Date nascimentoResponsavel) {
	this.nascimentoResponsavel = nascimentoResponsavel;
}
public String getSexoResponsavel() {
	return sexoResponsavel;
}
public void setSexoResponsavel(String sexoResponsavel) {
	this.sexoResponsavel = sexoResponsavel;
}
public String getEmailResponsavel() {
	return emailResponsavel;
}
public void setEmailResponsavel(String emailResponsavel) {
	this.emailResponsavel = emailResponsavel;
}
public String getCargo() {
	return cargo;
}
public void setCargo(String cargo) {
	this.cargo = cargo;
}
public String getCelularResponsavel() {
	return celularResponsavel;
}
public void setCelularResponsavel(String celularResponsavel) {
	this.celularResponsavel = celularResponsavel;
}
public String getWhatsAppResponsavel() {
	return WhatsAppResponsavel;
}
public void setWhatsAppResponsavel(String whatsAppResponsavel) {
	WhatsAppResponsavel = whatsAppResponsavel;
}

}
