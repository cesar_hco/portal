package com.Portal.modelo;

import java.io.Serializable;

import com.Portal.dao.Super;

public class Produto extends Super implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8941780672036491226L;
	public int idProduto;
	public String descricao;
	public String tamanho;
	public Double precobase;
	public String nomeProduto;
	
	public Produto(){
		super();
	}

	
	public int getIdProduto() {
		return idProduto;
	}


	public void setIdProduto(int idProduto) {
		this.idProduto = idProduto;
	}


	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getTamanho() {
		return tamanho;
	}

	public void setTamanho(String tamanho) {
		this.tamanho = tamanho;
	}

	
	public Double getPrecobase() {
		return precobase;
	}

	public void setPrecobase(Double precobase) {
		this.precobase = precobase;
	}

	public String getNomeProduto() {
		return nomeProduto;
	}

	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}

}
