package com.Portal.modelo;

public class Medida {

	public Integer idMedida;
	public String descricaoMedida;
	public String siglaMedida;
	
	
	public Integer getIdMedida() {
		return idMedida;
	}
	public void setIdMedida(Integer idMedida) {
		this.idMedida = idMedida;
	}
	public String getDescricaoMedida() {
		return descricaoMedida;
	}
	public void setDescricaoMedida(String descricaoMedida) {
		this.descricaoMedida = descricaoMedida;
	}
	public String getSiglaMedida() {
		return siglaMedida;
	}
	public void setSiglaMedida(String siglaMedida) {
		this.siglaMedida = siglaMedida;
	}
	
}
