package com.Portal.modelo;

import java.io.Serializable;
import java.util.Date;

public class Orcamento implements Serializable{


	
	private static final long serialVersionUID = 1L;

	public Integer idOrcamento;

	public Produto produtos;
	public Date dataOrcamento;
	public Cliente cliente;
	public double valorOrcamento;
	public int prazoEntregaOrcamento;
	public String observacaoOrcamento;
	public boolean realOrcamento;
	public boolean porcentagemOrcamento;
	public double descontoOrcamento;
	public double acrescimoOrcamento;
	public Date diaEntregaOrcamento;
	public boolean autorizarOrcamento;
	public String situacaoOrcamento;
	
	public String condicaoPagamentoOrcamento;
	public ListaProduto listaproduto;
	
	
	
	public ListaProduto getListaproduto() {
		return listaproduto;
	}
	public void setListaproduto(ListaProduto listaproduto) {
		this.listaproduto = listaproduto;
	}
	public int getIdOrcamento() {		
		
		return idOrcamento;
	}
	public void setIdOrcamento(int idOrcamento) {
		this.idOrcamento = idOrcamento;
	}
	public Produto getProdutos() {
		return produtos;
	}
	public void setProdutos(Produto produtos) {
		this.produtos = produtos;
	}
	public Date getDataOrcamento() {
		return dataOrcamento;
	}
	public void setDataOrcamento(Date dataOrcamento) {
		this.dataOrcamento = dataOrcamento;
	}
	
	public Cliente getCliente() {
		if (cliente == null) {
			cliente = new Cliente();
		}
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public double getValorOrcamento() {
		return valorOrcamento;
	}
	public void setValorOrcamento(double valorOrcamento) {
		this.valorOrcamento = valorOrcamento;
	}
	public int getPrazoEntregaOrcamento() {
		return prazoEntregaOrcamento;
	}
	public void setPrazoEntregaOrcamento(int prazoEntregaOrcamento) {
		this.prazoEntregaOrcamento = prazoEntregaOrcamento;
	}
	public String getObservacaoOrcamento() {
		return observacaoOrcamento;
	}
	public void setObservacaoOrcamento(String observacaoOrcamento) {
		this.observacaoOrcamento = observacaoOrcamento;
	}
	public boolean isRealOrcamento() {
		return realOrcamento;
	}
	public void setRealOrcamento(boolean realOrcamento) {
		this.realOrcamento = realOrcamento;
	}
	public boolean isPorcentagemOrcamento() {
		return porcentagemOrcamento;
	}
	public void setPorcentagemOrcamento(boolean porcentagemOrcamento) {
		this.porcentagemOrcamento = porcentagemOrcamento;
	}
	public double getDescontoOrcamento() {
		return descontoOrcamento;
	}
	public void setDescontoOrcamento(double descontoOrcamento) {
		this.descontoOrcamento = descontoOrcamento;
	}
	public double getAcrescimoOrcamento() {
		return acrescimoOrcamento;
	}
	public void setAcrescimoOrcamento(double acrescimoOrcamento) {
		this.acrescimoOrcamento = acrescimoOrcamento;
	}
	public Date getDiaEntregaOrcamento() {
		return diaEntregaOrcamento;
	}
	public void setDiaEntregaOrcamento(Date diaEntregaOrcamento) {
		this.diaEntregaOrcamento = diaEntregaOrcamento;
	}
	public boolean isAutorizarOrcamento() {
		return autorizarOrcamento;
	}
	public void setAutorizarOrcamento(boolean autorizarOrcamento) {
		this.autorizarOrcamento = autorizarOrcamento;
	}
	public String getSituacaoOrcamento() {
		return situacaoOrcamento;
	}
	public void setSituacaoOrcamento(String situacaoOrcamento) {
		this.situacaoOrcamento = situacaoOrcamento;
	}
	
	public String getCondicaoPagamentoOrcamento() {
		return condicaoPagamentoOrcamento;
	}
	public void setCondicaoPagamentoOrcamento(String condicaoPagamentoOrcamento) {
		this.condicaoPagamentoOrcamento = condicaoPagamentoOrcamento;
	}
	
	
	










}
