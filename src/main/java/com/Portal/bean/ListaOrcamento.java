package com.Portal.bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import com.Portal.dao.Orcamentodao;
import com.Portal.modelo.ListaProduto;
import com.Portal.modelo.Orcamento;

@ManagedBean(name = "MBTeste")
public class ListaOrcamento {

	private List<Orcamento> listaOrcamento;
	private Orcamento orcamento ;
	private ArrayList<ListaProduto> itensProduto  ;
	
	
	public Orcamento getOrcamento() {
		
		if (orcamento == null) {
			orcamento = new Orcamento();
		}
		
		return orcamento;
	}



	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}



	public ArrayList<ListaProduto> getItensProduto() {
		
		if (itensProduto == null) {
			
			itensProduto =  new  ArrayList<ListaProduto>();
		}
		
		
		return itensProduto;
	}



	public void setItensProduto(ArrayList<ListaProduto> itensProduto) {
		this.itensProduto = itensProduto;
	}



	public List<Orcamento> getListaOrcamento() {
		return listaOrcamento;
	}



	public void setListaOrcamento(List<Orcamento> listaOrcamento) {
		this.listaOrcamento = listaOrcamento;
	}



	@PostConstruct
	public void buscaLista() {

		try {

			Orcamentodao lista = new Orcamentodao();	
			
			listaOrcamento =  new ArrayList<Orcamento>();
			
			listaOrcamento = (ArrayList<Orcamento>) lista.listarorcamento();
						
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
public void salvar() {
		
		try {
			
			Orcamentodao orcamentodao = new Orcamentodao();
			
			orcamentodao.salvar(orcamento, itensProduto);
											
			FacesMessage msg = new FacesMessage("Cadastro Realizado com Sucesso !");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			
		} catch (Exception editar) {
			editar.printStackTrace();
		}
}
public String limparTabelaProduto() {
	
	FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("MBOrcamento");
	
	FacesMessage msg = new FacesMessage("Cadastro Realizado com Sucesso !");
	FacesContext.getCurrentInstance().addMessage(null, msg);
	
	return "Teste.xhtml?faces-redirect=true";

	}
	
}
