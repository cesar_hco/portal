package com.Portal.bean;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import com.Portal.conexao.MensagemAlertaEnum;
import com.Portal.dao.ProdutoDao;
import com.Portal.dao.Super;
import com.Portal.modelo.Produto;

@ManagedBean(name = "MBProduto")
@SessionScoped
public class Produtobean extends Super implements Serializable {

	private static final long serialVersionUID = 1L;
	private ArrayList<Produto> itensProduto;
	@ManagedProperty(value = "#{produtos}")
	private Produto produtos;
	
	public Produtobean() {
		super();
	}

	public ArrayList<Produto> getItensProduto() {
		return itensProduto;
	}

	public void setItensProduto(ArrayList<Produto> itensProduto) {
		this.itensProduto = itensProduto;
	}

	public Produto getProdutos() {
		if (produtos == null) {

			produtos = new Produto();
		}
		return produtos;
	}

	public void setProdutos(Produto produtos) {
		this.produtos = produtos;
	}
	
	public void PrepararPesquisa() {

		ProdutoDao Dao = new ProdutoDao();

		itensProduto = (ArrayList<Produto>) Dao.Consultar("DESCRICAO", getnome());

}
	public void salvar() {

		if (produtos.idProduto != 0) {
			
			try {

				ProdutoDao editardao = new ProdutoDao();
				editardao.alterar(produtos);

				ArrayList<Produto> lista = (ArrayList<Produto>) editardao.ConsultaCompleta();

				FacesMessage msg = new FacesMessage("Cadastro Editado com Sucesso !");
				FacesContext.getCurrentInstance().addMessage(null, msg);

			} catch (Exception editar) {
				editar.printStackTrace();
			}
			
		}else
		
		try {
			ProdutoDao produtodao = new ProdutoDao();
			produtodao.salvar(produtos);
			FacesMessage msg = new FacesMessage("Cadastro Realizado com Sucesso !");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public String prepararSalvar() {

		produtos = new Produto();

		return "AddProduto.xhtml";
	}
	
	
	public String limparTabelaProduto() {
		
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("MBProduto");		
		
		return "Produto.xhtml?faces-redirect=true";
	
		}

	public void prepararexcluir() {

	}

	public String excluir(ActionEvent evento) {
		try {
			
			produtos = (Produto) evento.getComponent().getAttributes().get("produtoSelecionado");
			ProdutoDao dao = new ProdutoDao();
			dao.remover(produtos);
						
		/*	FacesMessage msg = new FacesMessage("Cadastro Excluido com Sucesso !");
			FacesContext.getCurrentInstance().addMessage(null, msg);*/
			
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensagem Teste","Merda"));
			
			

		} catch (Exception excluir) {
			excluir.printStackTrace();
		}
		return "Produto.xhtml?faces-redirect=true";
	}


	public void editar() {

		try {

			ProdutoDao editardao = new ProdutoDao();
			editardao.alterar(produtos);

			ArrayList<Produto> lista = (ArrayList<Produto>) editardao.ConsultaCompleta();

			FacesMessage msg = new FacesMessage("Cadastro Editado com Sucesso !");
			FacesContext.getCurrentInstance().addMessage(null, msg);

		} catch (Exception editar) {
			editar.printStackTrace();
		}

	}

	public void prepararEditar(ActionEvent evento) {
	produtos = (Produto) evento.getComponent().getAttributes().get("produtoSelecionado");
		
		
		}
	@PostConstruct
	public void VerificarLogar() {
	 if (getUsuario() != null) {
		
	} 
	}

}
