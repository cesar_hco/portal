package com.Portal.bean;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import com.Portal.dao.ClienteDao;
import com.Portal.dao.Super;
import com.Portal.dao.usuarioDao;
import com.Portal.modelo.Cliente;
import com.Portal.modelo.Usuario;

@ManagedBean(name = "MBUsuario")
@SessionScoped
public class Usuariobean extends Super implements Serializable {
	
	public Usuariobean() {
		super();
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ArrayList<Usuario> itensUsuarios;
    private Usuario usuarios;
	
	public ArrayList<Usuario> getItensUsuarios() {
		return itensUsuarios;
	}

	public void setItensUsuarios(ArrayList<Usuario> itensUsuarios) {
		this.itensUsuarios = itensUsuarios;
	}

	
	
	

	public Usuario getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(Usuario usuarios) {
		this.usuarios = usuarios;
	}

	public void salvar() {
		if (usuarios.getIdUsuario() !=0) {
			
			try {
				usuarioDao usuariodao = new usuarioDao();
				usuariodao.alterar(usuarios);
				FacesMessage msg = new FacesMessage("Cadastro Editado com Sucesso !");
				FacesContext.getCurrentInstance().addMessage(null, msg);
				
			} catch (Exception editar) {
				editar.printStackTrace();
			}
		}else {
			
			try {
				usuarioDao usuariodao = new usuarioDao();
				usuariodao.salvar(usuarios);
				FacesMessage msg = new FacesMessage("Cadastro Realizado com Sucesso !");
				FacesContext.getCurrentInstance().addMessage(null, msg);
			} catch (Exception e) {
				FacesMessage msg = new FacesMessage("Erro ao Cadastrar!");
				FacesContext.getCurrentInstance().addMessage(null, msg);
				e.printStackTrace();
			}
		}
		
	}
	public void prepararSalvar() {
		usuarios = new Usuario();		
	}
	
	@PostConstruct
	public void Listar() {

		try {

			usuarioDao usuarioDao = new usuarioDao();
			itensUsuarios = (ArrayList<Usuario>) usuarioDao.ConsultarFuncioanrio(getnome());
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	public void prepararEditar(ActionEvent evento) {
		usuarios = (Usuario) evento.getComponent().getAttributes().get("usuarioSelecionado");
		}
	
	
	public void excluir(ActionEvent evento) {
		try {
			
			usuarios = (Usuario) evento.getComponent().getAttributes().get("usuarioSelecionado");
			usuarioDao usuariodao = new usuarioDao();
			usuariodao.excluir(usuarios);

			itensUsuarios = (ArrayList<Usuario>) usuariodao.ConsultarFuncioanrio(getnome());
			

			FacesMessage msg = new FacesMessage("Cadastro Excluido com Sucesso !");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
