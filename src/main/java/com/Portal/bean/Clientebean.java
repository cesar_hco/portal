package com.Portal.bean;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import com.Portal.dao.ClienteDao;
import com.Portal.modelo.Cliente;

@ManagedBean(name = "MBCliente")
public class Clientebean implements Serializable {

	private static final long serialVersionUID = 1L;
	private ArrayList<Cliente> itensClientes;
	private ArrayList<Cliente> itensfiltro;
	
	
	public ArrayList<Cliente> getItensClientes() {
		return itensClientes;
	}

	public void setItensClientes(ArrayList<Cliente> itensClientes) {
		this.itensClientes = itensClientes;
	}

	public ArrayList<Cliente> getItensfiltro() {
		return itensfiltro;
	}

	public void setItensfiltro(ArrayList<Cliente> itensfiltro) {
		this.itensfiltro = itensfiltro;
	}


	private Cliente cliente;
	
	



	public Cliente getCliente() {
		if (cliente == null) {
			cliente = new Cliente();
		}
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	

	public void salvar() {

	
		if (cliente.idCliente != 0 ) {
			
			
			try {
								
				ClienteDao clientedao = new ClienteDao();
				clientedao.alterar(cliente);

				itensClientes = clientedao.listar();
				

				FacesMessage msg = new FacesMessage("Cadastro Editado com Sucesso !");
				FacesContext.getCurrentInstance().addMessage(null, msg);
			} catch (Exception editar) {
				editar.printStackTrace();
			}
			
						
		}	else{

		
			try {
				ClienteDao clientedao = new ClienteDao();
				clientedao.salvar(cliente);
				FacesMessage msg = new FacesMessage("Cadastro Realizado com Sucesso !");
				FacesContext.getCurrentInstance().addMessage(null, msg);
				
			} catch (Exception e) {
				FacesMessage msg = new FacesMessage("Entrar em contato com o Tercio!");
				FacesContext.getCurrentInstance().addMessage(null, msg);
				e.printStackTrace();
				
			}
				
		}
		
	}
		
			

	public void prepararSalvar() {

		cliente = new Cliente();

		
	}

	@PostConstruct
	public void Listar() {

		try {

			ClienteDao clientedao = new ClienteDao();
			itensClientes = (ArrayList<Cliente>) clientedao.listar();
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void prepararEditar(ActionEvent evento) {
	cliente = (Cliente) evento.getComponent().getAttributes().get("clienteSelecionado");
	}
	
	
	public void excluir(ActionEvent evento) {
		try {
			
			cliente = (Cliente) evento.getComponent().getAttributes().get("clienteSelecionado");
			ClienteDao clientedao = new ClienteDao();
			clientedao.excluir(cliente);

			itensClientes = clientedao.listar();
			

			FacesMessage msg = new FacesMessage("Cadastro Excluido com Sucesso !");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	

	

}
