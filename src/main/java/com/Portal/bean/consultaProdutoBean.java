package com.Portal.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.event.ActionEvent;

import com.Portal.dao.ProdutoDao;
import com.Portal.dao.Super;
import com.Portal.modelo.ListaProduto;
import com.Portal.modelo.Produto;

@ManagedBean(name = "MBconsultaProduto")
public class consultaProdutoBean  extends Super implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	public ArrayList<Produto> itensProdutos;
	public ArrayList<ListaProduto> listaProduto = new  ArrayList<ListaProduto>();;

	public ArrayList<Produto> getItensProdutos() {
		return itensProdutos;
	}

	public void setItensProdutos(ArrayList<Produto> itensProdutos) {
		this.itensProdutos = itensProdutos;
	}
	
	
	@PostConstruct
	public void ListarProduto() {

		ProdutoDao Dao = new ProdutoDao();

		itensProdutos = (ArrayList<Produto>) Dao.Consultar("DESCRICAO", getnome());
		
	}
	
	public void adicionar(ActionEvent evento) {

		Produto produto = (Produto) evento.getComponent().getAttributes().get("AdicionarProduto");
				
			
		
		
		
		ListaProduto listaProdutos = new ListaProduto();

			listaProdutos.setIdlistaProduto(0);			
			listaProdutos.setId_produtos(produto);
			listaProdutos.setQuantidadeProduto(1);
			
			listaProduto.add(listaProdutos);
			
		
			
	
	}
	
}
