package com.Portal.bean;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.ListDataModel;

import com.Portal.dao.Funcionariodao;
import com.Portal.modelo.Funcionario;

@ManagedBean(name = "MBFuncionario")

public class Funcionariobean implements Serializable {

	private static final long serialVersionUID = 1L;
	private ListDataModel<Funcionario> itensFuncionario;
	private Funcionario funcionario = new Funcionario();

	public ListDataModel<Funcionario> getItensFuncionario() {
		return itensFuncionario;
	}

	public void setItensFuncionario(ListDataModel<Funcionario> itensFuncionario) {
		this.itensFuncionario = itensFuncionario;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public void salvar() {

		if (funcionario.idfuncionario != 0) {
			try {
				Funcionariodao funcionariodao = new Funcionariodao();
				funcionariodao.alterar(funcionario);

				ArrayList<Funcionario> lista = funcionariodao.listar();
				itensFuncionario = new ListDataModel<Funcionario>(lista);

				FacesMessage msg = new FacesMessage("Cadastro Editado com Sucesso !");
				FacesContext.getCurrentInstance().addMessage(null, msg);

			} catch (Exception editar) {
				editar.printStackTrace();
			}

		} else {

			try {

				Funcionariodao funcionariodao = new Funcionariodao();
				funcionariodao.salvar(funcionario);
				FacesMessage msg = new FacesMessage("Cadastro Realizado com Sucesso !");
				FacesContext.getCurrentInstance().addMessage(null, msg);

			} catch (Exception ex) {
				
				ex.printStackTrace();
			}finally {
				funcionario = new Funcionario();
			}
		}
	}
		

	

	public void prepararSalvar() {

		funcionario = new Funcionario();
	}

	

	
	@PostConstruct
	public void buscar() {
		
		
		
		try {
			Funcionariodao funcionariodao = new Funcionariodao();
			ArrayList<Funcionario> lista = funcionariodao.listar();
			itensFuncionario = new ListDataModel<Funcionario>(lista);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void prepararEditar(ActionEvent evento){
		
		funcionario = (Funcionario) evento.getComponent().getAttributes().get("funcionarioSelecionado");
		
		
	}
	public void Excluir(ActionEvent evento){
		
		try {
			
			funcionario = (Funcionario) evento.getComponent().getAttributes().get("funcionarioSelecionado");
			
			Funcionariodao funcionariodao = new Funcionariodao();
			funcionariodao.excluir(funcionario);
			
			ArrayList<Funcionario> lista = funcionariodao.listar();
			itensFuncionario = new ListDataModel<Funcionario>(lista);

			FacesMessage msg = new FacesMessage("Cadastro Excluido com Sucesso !");
			FacesContext.getCurrentInstance().addMessage(null, msg);	
			
		} catch (Exception e) {
			FacesMessage msg = new FacesMessage("Erro ao Excluir !");
			FacesContext.getCurrentInstance().addMessage(null, msg);	
			e.printStackTrace();
		}
		
	}
	}

