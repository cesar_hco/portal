package com.Portal.bean;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.Portal.dao.Loguindao;
import com.Portal.dao.Super;
import com.Portal.dao.usuarioDao;
import com.Portal.modelo.Usuario;



@ManagedBean(name = "MBLoguin")
@SessionScoped
public class Loguinbean extends Super implements Serializable {

	private Usuario usuarios;
	private ArrayList<Usuario> ListaUsuarios;
	
	
	public Loguinbean() {
		super();
	}
	
	public Usuario getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(Usuario usuarios) {
		this.usuarios = usuarios;
	}

	public ArrayList<Usuario> getListaUsuarios() {
		return ListaUsuarios;
	}

	public void setListaUsuarios(ArrayList<Usuario> listaUsuarios) {
		ListaUsuarios = listaUsuarios;
	}

	public void Logar() throws SQLException {
		
		try {
							
		Loguindao loguindao = new Loguindao();
		
		ListaUsuarios = (ArrayList<Usuario>) loguindao.logar(getUsuario() , getSenha());
		
		if (ListaUsuarios.size() == 1) {						
				FacesContext.getCurrentInstance().getExternalContext().redirect("MenuPrincipal.xhtml");
				FacesMessage msg = new FacesMessage("Entrar em contato com o Tercio!");
				FacesContext.getCurrentInstance().addMessage(null, msg);
			
				
		}else{
			FacesMessage msg = new FacesMessage("Usuario ou senha errado !");
			FacesContext.getCurrentInstance().addMessage(getMensagem(), msg);			
			
		}
		} catch (IOException e) {
			
			e.printStackTrace();
		}		
		
	}
	
	
}
