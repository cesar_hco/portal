package com.Portal.bean;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import com.Portal.dao.ClienteDao;
import com.Portal.dao.Orcamentodao;
import com.Portal.dao.ProdutoDao;
import com.Portal.dao.Super;
import com.Portal.modelo.Cliente;
import com.Portal.modelo.ListaProduto;
import com.Portal.modelo.Orcamento;
import com.Portal.modelo.Produto;

@ManagedBean(name = "MBOrcamento")
@SessionScoped
public class Orcamentobean extends Super implements Serializable{


	private static final long serialVersionUID = 1L;
		
	private ArrayList<ListaProduto> itensProduto  ;
	private List<Orcamento> listaOrcamento;
	private List<ListaProduto> itensProdutos  ;
	private List<Produto> produtosselecionados;
	private ArrayList<Cliente> itensClientes;
	private Cliente cliente;
	private List<Cliente> itensfiltro;
	private Orcamento orcamento ;
	private ListaProduto lista;
	
	public Orcamentobean() {
		super();
	}
	
	
	public List<Orcamento> getListaOrcamento() {
		return listaOrcamento;
	}

	public void setListaOrcamento(List<Orcamento> listaOrcamento) {
		this.listaOrcamento = listaOrcamento;
	}

	public List<ListaProduto> getItensProdutos() {
		return itensProdutos;
	}

	public void setItensProdutos(List<ListaProduto> itensProdutos) {
		this.itensProdutos = itensProdutos;
	}

	public ListaProduto getLista() {
		return lista;
	}
	
	public ListaProduto Orcamento() {
		return lista;
	}

	public void setLista(ListaProduto lista) {
		this.lista = lista;
	}

	


	public ArrayList<ListaProduto> getItensProduto() {
		
		if (itensProduto == null) {
			
			itensProduto =  new  ArrayList<ListaProduto>();
		}
		
		return itensProduto;
	}

	public void setItensProduto(ArrayList<ListaProduto> itensProduto) {
		this.itensProduto = itensProduto;
	}

	public List<Produto> getProdutosselecionados() {
		return produtosselecionados;
	}

	public void setProdutosselecionados(List<Produto> produtosselecionados) {
		this.produtosselecionados = produtosselecionados;
	}
	

	public ArrayList<Cliente> getItensClientes() {
		return itensClientes;
	}

	public void setItensClientes(ArrayList<Cliente> itensClientes) {
		this.itensClientes = itensClientes;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<Cliente> getItensfiltro() {
		return itensfiltro;
	}

	public void setItensfiltro(List<Cliente> itensfiltro) {
		this.itensfiltro = itensfiltro;
	}

	public Orcamento getOrcamento() {
		if (orcamento == null) {
			orcamento = new Orcamento();
		}
		return orcamento;
	}

	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}



	public void adicionar(ActionEvent evento) {

		Produto produto = (Produto) evento.getComponent().getAttributes().get("AdicionarProduto");
		
		int produtoSelecionado = produto.getIdProduto();
		
		
		
	boolean ProdutoExistente = false;		
		
			if (itensProduto.size() == 0) {
				
				ListaProduto listaProdutos = new ListaProduto();

				listaProdutos.setIdlistaProduto(0);			
				listaProdutos.setId_produtos(produto);
				listaProdutos.setQuantidadeProduto(1);
				
				itensProduto.add(listaProdutos);
				
			}else{
				
				for (Iterator iterator = itensProduto.iterator(); iterator.hasNext(); ) {  
					ListaProduto idProduto = (ListaProduto) iterator.next();  
					  
					if (idProduto.getId_produtos().getIdProduto() == produtoSelecionado) {
						ProdutoExistente = true;
					}	 
				}
					if(ProdutoExistente == false){
					
					ListaProduto listaProdutos = new ListaProduto();

					listaProdutos.setIdlistaProduto(0);			
					listaProdutos.setId_produtos(produto);
					listaProdutos.setQuantidadeProduto(1);
					
					itensProduto.add(listaProdutos);
					
				}
			}		
		}					
	public void BuscarProduto() {

		try {

			ProdutoDao Dao = new ProdutoDao();
			produtosselecionados = Dao.ConsultaCompleta();
} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	public void buscaCliente() {

		try {

			ClienteDao clientedao = new ClienteDao();
			itensClientes = (ArrayList<Cliente>) clientedao.listar();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void buscaClienteCodigo() {

		try {

			ClienteDao clientedao = new ClienteDao();
			itensClientes = (ArrayList<Cliente>) clientedao.buscarCodigo(getCodigo());
									
			cliente = itensClientes.get(getCodigo());
			
			Orcamento clienteOrcamento = new Orcamento();
			
			clienteOrcamento.setCliente(cliente);
			
			orcamento.setCliente(clienteOrcamento.getCliente());
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void listaCliente(ActionEvent evento) {
		

		cliente = (Cliente) evento.getComponent().getAttributes().get("clienteSelecionados");
		
		Orcamento clienteOrcamento = new Orcamento();
		
		clienteOrcamento.setCliente(cliente);
		
		orcamento.setCliente(clienteOrcamento.getCliente());
}
	public void removerLista(ActionEvent evento){
		
		ListaProduto listaProduto = (ListaProduto) evento.getComponent().getAttributes().get("removerLista");
		
	int produtoSelecionado = listaProduto.getId_produtos().getIdProduto();
		int linhaProduto = -1;
		
		for(int posicao = 0 ; posicao < itensProduto.size(); posicao ++  ){
						
			if (itensProduto.get(posicao).getId_produtos().equals(listaProduto.getId_produtos())){
				linhaProduto = posicao;
	}
			
		}
		
		if (linhaProduto > -1) {
			itensProduto.remove(linhaProduto);
		}
	}
	public void salvar() {
		
		try {
			
			Orcamentodao orcamentodao = new Orcamentodao();
			
			orcamentodao.salvar(orcamento, itensProduto);
											
			FacesMessage msg = new FacesMessage("Cadastro Realizado com Sucesso !");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			
		} catch (Exception editar) {
			editar.printStackTrace();
		}
}
	public void prepararSalvar(){
		
		orcamento = new Orcamento();
		itensProduto =  new  ArrayList<ListaProduto>();
		cliente = new Cliente();
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("Teste.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
	@PostConstruct
	public void buscaLista() {

		try {

			Orcamentodao lista = new Orcamentodao();	
			
			listaOrcamento =  new ArrayList<Orcamento>();
			
			listaOrcamento = (ArrayList<Orcamento>) lista.listarorcamento();
						
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	public String Excluir(ActionEvent evento){
		
		try {
			
			orcamento = (Orcamento) evento.getComponent().getAttributes().get("orcamentoSelecionado");			
			Orcamentodao orcamentodao = new Orcamentodao();
			orcamentodao.excluir(orcamento);			

			FacesMessage msg = new FacesMessage("Cadastro Excluido com Sucesso !");
			FacesContext.getCurrentInstance().addMessage(null, msg);	
			
		} catch (Exception e) {
			FacesMessage msg = new FacesMessage("Erro ao Excluir !");
			FacesContext.getCurrentInstance().addMessage(null, msg);	
			e.printStackTrace();
		}
		return "Orcamentos.xhtml?faces-redirect=true";
	}

	
	public String limparTabelaProduto() {
		
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("MBOrcamento");
		
		FacesMessage msg = new FacesMessage("Cadastro Realizado com Sucesso !");
		FacesContext.getCurrentInstance().addMessage(null, msg);
		
		return "Teste.xhtml?faces-redirect=true";
	
		}
	
	public void prepararEditar(ActionEvent evento) {
		
		try {
			orcamento = (Orcamento) evento.getComponent().getAttributes().get("orcamentoSelecionado");
			FacesContext.getCurrentInstance().getExternalContext().redirect("Teste.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
		

}
